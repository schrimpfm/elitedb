\ProvidesClass{mathclass}
\LoadClass[11pt,a4paper,oneside]{scrartcl}

\usepackage[utf8]{inputenc} % Kodierung
\usepackage[ngerman]{babel} % Sprache
\usepackage[ngerman]{babel}
\usepackage{ngerman}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage{times}
\usepackage{alltt}
\usepackage{moreverb}
\usepackage[T1]{fontenc}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{enumitem}



% skip between paragraphs
\setlength{\parskip}{1ex}
% ... and no indentation at start of a new paragraph
\setlength{\parindent}{0ex}


\renewcommand\theenumi{\alph{enumi}}
\renewcommand\labelenumi{\theenumi)}

\newcommand\skipitem{\stepcounter{enumi}}

\pagestyle{fancy}
\thispagestyle{empty}

% -----------------------------------------------------------------------
% Main macros

\renewcommand{\maketitle}[8]
          {\@maketitle{#1}{#2}{#3}{#4}{#5}{#6}{#7}{#8}}
\newcommand\skipsection{\stepcounter{section}}



\renewcommand{\@maketitle}[9]
{\begin{tabular}{p{7cm}|r}

\begin{tabular}{l}
{#1} \textit{\scriptsize{{#2}}} \\
{#3} \textit{\scriptsize{{#4}}} \\ 
{#5} \textit{\scriptsize{{#6}}} 
\end{tabular}
&
\begin{tabular}{ll}
Tutor: & \thistutor \\
Abgabedatum: &  {#7}
\end{tabular}
\end{tabular}

\vspace{15pt}

\Large{\textbf{\thiscourse} (\thissemester)}

 \rule{\linewidth}{1pt}

\normalsize
\noindent
    \fancyhead{} %clear all fields
    \fancyhead[LO,RE]{\thiscourseshort, \thissemester}
    \fancyhead[RO,LE]{Gruppe: \textit{{#1}}}
}

\endinput
