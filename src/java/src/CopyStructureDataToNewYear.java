import java.sql.*;

/**
 * @author Martin
 * @created 22.12.2014
 */
public class CopyStructureDataToNewYear {
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			printUsage();
			System.exit(0);
		}

		String host = "soflu.de", db = "elections", user = args[0], pass = args[1];
		int port = 5432;
		if (args.length > 2) {
			host = args[2];
		}

		final CopyStructureDataToNewYear copyProgram = new CopyStructureDataToNewYear(host, port, db, user, pass);
		copyProgram.run(2017);
	}

	private static void printUsage() {
		System.out.println("Usage: java CopyWahlkreiseToNewYear.java <user> <password> [<host=soflu.de>]");
	}

	private final Connection conn;
	private final PreparedStatement yearCheck, createYear,
			copyBundeslaender, copyWahlkreise,
			copyParties, copyCandidates,
			copyLandeslisten, copyWahlkreisliste,
			countLandeslistenOld, countLandeslistenNew,
			countWahlkreislisteOld, countWahlkreislisteNew;

	public CopyStructureDataToNewYear(final String host, final int port, final String db, final String user,
									  final String pass) throws SQLException {
		final String connectionString = "jdbc:postgresql://" + host + ":" + port + "/"
				+ db + "?" + "user=" + user + "&password=" + pass;
		System.out.println("Connecting with " + connectionString);
		conn = DriverManager.getConnection(connectionString);

		yearCheck = conn.prepareStatement("SELECT TRUE FROM wahljahr WHERE jahr = ? - 4");
		createYear = conn.prepareStatement("INSERT INTO wahljahr(jahr) VALUES (?)");
		copyBundeslaender = conn.prepareStatement("INSERT INTO bundesland(name, einwohner, jahr)" +
				" SELECT name, einwohner, ?" +
				" FROM bundesland" +
				" WHERE jahr = ? - 4");
		copyWahlkreise = conn.prepareStatement(
				"INSERT INTO wahlkreis(wahlkreisnummer, name, bundesland_id, wahlberechtigte, jahr)" +
						" SELECT wahlkreisnummer, name, " +
						"		(SELECT id FROM bundesland WHERE name = " +
						"			(SELECT name FROM bundesland WHERE id = bundesland_id)" +
						"			AND jahr = ?), " + // selbes Bundesland im aktuellen Jahr
						"		wahlberechtigte, ?" +
						" FROM wahlkreis" +
						" WHERE jahr = ? - 4");
		copyParties = conn.prepareStatement("INSERT INTO partei(name, jahr)" +
				" SELECT name, ? FROM partei WHERE jahr = ? - 4");
		copyCandidates = conn.prepareStatement("INSERT INTO kandidat(vor_name, nach_name, geburtsjahr, partei_id)" +
				" SELECT vor_name, nach_name, geburtsjahr, " +
				"		(SELECT id FROM partei WHERE name = (SELECT name FROM partei WHERE id = partei_id AND jahr = ? - 4) AND jahr = ?)" +
				" FROM kandidat" +
				" WHERE partei_id IN (SELECT id FROM partei WHERE jahr = ? - 4)");
		copyLandeslisten =
				conn.prepareStatement("INSERT INTO landesliste(bundesland_id, partei_id, prio, kandidat_id)" +
						" SELECT " +
						"	(SELECT id FROM bundesland WHERE name = (SELECT name FROM bundesland WHERE id = bundesland_id AND jahr = ? - 4) AND jahr = ?), " +
						"	(SELECT id FROM partei WHERE name = (SELECT name FROM partei WHERE id = partei_id AND jahr = ? - 4) AND jahr = ?), " +
						"	prio, " +
						"	(SELECT id FROM kandidat " +
						"		WHERE vor_name = (SELECT vor_name FROM kandidat WHERE id = kandidat_id)" +
						"		AND nach_name = (SELECT nach_name FROM kandidat WHERE id = kandidat_id)" +
						"		AND geburtsjahr = (SELECT geburtsjahr FROM kandidat WHERE id = kandidat_id)" +
						" 		AND kandidat.partei_id = (SELECT id FROM partei WHERE name = (SELECT name FROM partei WHERE id = landesliste.partei_id AND jahr = ? - 4) AND jahr = ?))" +
						" FROM landesliste" +
						" WHERE bundesland_id IN (SELECT id FROM bundesland WHERE jahr = ? - 4)");
		copyWahlkreisliste = conn.prepareStatement("INSERT INTO wahlkreisliste(wahlkreis_id, kandidat_id)" +
				" SELECT" +
				" 	(SELECT id FROM wahlkreis WHERE name = (SELECT name FROM wahlkreis WHERE id = wahlkreis_id AND jahr = ? - 4) AND jahr = ?), " +
				" 	(SELECT MAX(id) FROM kandidat " + // highest id will be the most recent year
				"		WHERE vor_name = (SELECT vor_name FROM kandidat WHERE id = kandidat_id)" +
				"		AND nach_name = (SELECT nach_name FROM kandidat WHERE id = kandidat_id)" +
				"		AND geburtsjahr = (SELECT geburtsjahr FROM kandidat WHERE id = kandidat_id)" +
				" 		AND CASE WHEN (SELECT partei_id FROM kandidat WHERE id = kandidat_id) IS NULL " + // make sure we get the correct year
				"			THEN TRUE" + // filtered out by max clause
				"			ELSE kandidat.partei_id IN (SELECT id FROM partei WHERE jahr = ?)" +
				"			END" +
				"	)" +
				" FROM wahlkreisliste" +
				" WHERE wahlkreis_id IN (SELECT id FROM wahlkreis WHERE jahr = ? - 4)");

		countLandeslistenOld = conn.prepareStatement("SELECT COUNT(*)" +
				" FROM landesliste" +
				" WHERE bundesland_id IN (SELECT id FROM bundesland WHERE jahr = ? - 4)");
		countLandeslistenNew = conn.prepareStatement("SELECT COUNT(*)" +
				" FROM landesliste" +
				" WHERE bundesland_id IN (SELECT id FROM bundesland WHERE jahr = ?)");
		countWahlkreislisteOld = conn.prepareStatement("SELECT COUNT(*)" +
				" FROM wahlkreisliste" +
				" WHERE wahlkreis_id IN (SELECT id FROM wahlkreis WHERE jahr = ? - 4)");
		countWahlkreislisteNew = conn.prepareStatement("SELECT COUNT(*)" +
				" FROM wahlkreisliste" +
				" WHERE wahlkreis_id IN (SELECT id FROM wahlkreis WHERE jahr = ?)");
	}

	public void run(final int year) throws SQLException {
		yearCheck.setInt(1, year);
		ResultSet yearCheckResult = yearCheck.executeQuery();
		if (!yearCheckResult.next()) {
			System.out.println("Altes Jahr " + (year - 4) + " ist nicht im System vorhanden");
			return;
		}


		countLandeslistenOld.setInt(1, year);
		countLandeslistenNew.setInt(1, year);
		countWahlkreislisteOld.setInt(1, year);
		countWahlkreislisteNew.setInt(1, year);

		conn.setAutoCommit(false);
		PreparedStatement[] statements =
				{createYear,
						copyBundeslaender, copyWahlkreise,
						copyParties, copyCandidates,
						copyLandeslisten, copyWahlkreisliste};
		for (final PreparedStatement statement : statements) {
			ParameterMetaData parameterMetaData = statement.getParameterMetaData();
			final int parameterCount = parameterMetaData.getParameterCount();
			for (int i = 1; i <= parameterCount; i++) {
				statement.setInt(i, year);
			}
			System.out.println(statement.toString());
			statement.executeUpdate();
		}

		/* validate landes- and wahlkreisliste */
		if (!checkEquals("Landeslisten", countLandeslistenOld, countLandeslistenNew)) return;
		if (!checkEquals("Wahlkreislisten", countWahlkreislisteOld, countWahlkreislisteNew)) return;

		conn.commit();
	}

	private boolean checkEquals(String descriptor, PreparedStatement stmt1, PreparedStatement stmt2)
			throws SQLException {
		ResultSet rs1 = stmt1.executeQuery();
		ResultSet rs2 = stmt2.executeQuery();
		rs1.next();
		rs2.next();
		final int count1 = rs1.getInt(1);
		final int count2 = rs2.getInt(1);
		if (count1 != count2) {
			System.err.printf("%s count different (%d <-> %d)\n", descriptor, count1, count2);
			conn.rollback();
			return false;
		}
		return true;
	}
}
