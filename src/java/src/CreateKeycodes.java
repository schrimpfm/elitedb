import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by Patrick on 18.12.2014.
 */
public class CreateKeycodes {
    private final static int LENGTH_OF_KEYCODE = 16;
    private final static int NUMBER_OF_KEYCODES = 1000000;

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.exit(0);
        }

        String host = "soflu.de", db = "elections", user = args[0], pass = args[1];
        int port = 5432;
        if (args.length > 2) {
            host = args[2];
        }
        CreateKeycodes createKeycodes = new CreateKeycodes(host, port, db, user, pass);
        createKeycodes.run();
    }

    private final Statement stmt;
    private final Random random;
    private HashSet<String> keycodes;

    public CreateKeycodes(final String host, final int port, final String db, final String user, final String pass)
            throws SQLException, ClassNotFoundException {
        final String connectionString = "jdbc:postgresql://" + host + ":" + port + "/"
                + db + "?" + "user=" + user + "&password=" + pass;
        System.out.println("Connecting with " + connectionString);
        final Connection conn = DriverManager.getConnection(connectionString);
        stmt = conn.createStatement();
        random = new Random();
        keycodes = new HashSet<String>();
    }

    public void run() throws SQLException {
        if (NUMBER_OF_KEYCODES <= 0) return;
        int chunkSize = 1000;
        int numberOfChunks = (NUMBER_OF_KEYCODES - 1) / chunkSize + 1;
        int remainingKeycodes = NUMBER_OF_KEYCODES;
        for (int chunk = 0; chunk < numberOfChunks; chunk++) {
            StringBuilder insertQuery = new StringBuilder();
            insertQuery.append("INSERT INTO keycodes (keycode, status, wahlkreis_id) VALUES ");
            int currentChunk = Math.min(chunkSize, remainingKeycodes);
            for (int i = 0; i < currentChunk; i++) {
                if (i > 0) insertQuery.append(",");
                insertQuery.append("('" + getNewRandomKeycode() + "',0,null)");
            }
            stmt.execute(insertQuery.toString());
            remainingKeycodes -= currentChunk;
            System.out.println("Remeining: "+remainingKeycodes);
        }
        System.out.println("Finished insert");
    }

    private String getNewRandomKeycode() {
        String randomKeycode = getRandomKeycode();
        while (keycodes.contains(randomKeycode)) {
            randomKeycode = getRandomKeycode();
        }
        keycodes.add(randomKeycode);
        return randomKeycode;
    }

    private String getRandomKeycode() {
        char[] keycode = new char[LENGTH_OF_KEYCODE];
        for (int i = 0; i < keycode.length; i++) {
            keycode[i] = getRandomAlphanumericChar();
        }
        return new String(keycode);
    }

    private char getRandomAlphanumericChar() {
        int r = random.nextInt(36);
        if (r < 10) return (char) (r + '0');
        else return (char) (r - 10 + 'A');
    }
}
