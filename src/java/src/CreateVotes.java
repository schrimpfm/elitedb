import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class CreateVotes {

	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			printUsage();
			System.exit(0);
		}

		String host = "soflu.de", db = "elections", user = args[0], pass = args[1];
		int port = 5432;
		if (args.length > 2) {
			host = args[2];
		}
		CreateVotes createVotes = new CreateVotes(host, port, db, user, pass);
		createVotes.run();
	}

	private static void printUsage() {
		System.out.println("Usage: java CreateVotes.java <user> <password> [<host=soflu.de>]");
	}

	private static final String WAHLZETTEL_TRIGGERS = "Refresh_stimmen_aggregate",
			LIMIT = ""; // limit 3";

	private final Statement stmt;

	private String insertTableName = "wahlzettel";

	public CreateVotes(final String host, final int port, final String db, final String user, final String pass)
			throws SQLException {
		final String connectionString = "jdbc:postgresql://" + host + ":" + port + "/"
				+ db + "?" + "user=" + user + "&password=" + pass;
		System.out.println("Connecting with " + connectionString);
		final Connection conn = DriverManager.getConnection(connectionString);
		stmt = conn.createStatement();
	}

	public void run() throws SQLException {
		final Date startDate = new Date();
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println("Started on " + dateFormat.format(startDate));

		System.out.println(">> Disabling triggers " + WAHLZETTEL_TRIGGERS);
		stmt.execute("ALTER TABLE wahlzettel DISABLE TRIGGER " + WAHLZETTEL_TRIGGERS);

		System.out.println(">> Retrieving aggregated candidate voting data");
		Map<Integer, List<Candidate>> candidatesMap = getFirstVotes();

		System.out.println(">> Retrieving second votes");
		List<PartyVotes> partyVotes = getPartyVotes();

		System.out.println(">> 1st Run: Creating votes based on party results");
		createVotes(candidatesMap, partyVotes);

		System.out.println(">> 2nd Run: Fill up candidate votes");
		fillUpCandidateVotes(candidatesMap);

		System.out.println(">> Sanity Check");
		sanityCheck(partyVotes, candidatesMap);

		System.out.println(">> Re-enabling triggers " + WAHLZETTEL_TRIGGERS);
		stmt.execute("REFRESH MATERIALIZED VIEW Wahlkreis_partei_ergebnis;");
		stmt.execute("REFRESH MATERIALIZED VIEW Wahlkreis_kandidaten_ergebnis;");
		stmt.execute("ALTER TABLE wahlzettel ENABLE TRIGGER " + WAHLZETTEL_TRIGGERS);

		final Date endDate = new Date();
		final long durationSeconds = (endDate.getTime() - startDate.getTime()) / 1000;
		System.out.println("Finished on " + dateFormat.format(endDate) + " (" + durationSeconds + " seconds)");
	}

	private Map<Integer, List<Candidate>> getFirstVotes() throws SQLException {
		stmt.execute("SELECT wahlkreis_id, kandidat_id, stimmen" +
				" FROM erststimmen_aggregiert" +
				LIMIT + ";");
		ResultSet candidatesResult = stmt.getResultSet();

		int candidateCounter = 0, votesCounter = 0;
		Map<Integer, List<Candidate>> candidatesMap = new HashMap<Integer, List<Candidate>>();
		while (candidatesResult.next()) {
			final Candidate candidate = Candidate.from(candidatesResult);
			List<Candidate> list = candidatesMap.get(candidate.wahlkreisId);
			if (list == null) {
				list = new ArrayList<Candidate>();
				candidatesMap.put(candidate.wahlkreisId, list);
			}
			list.add(candidate);
			candidateCounter++;
			votesCounter += candidate.stimmen;
		}
		System.out.println("Found " + candidateCounter + " candidates with a total of " + votesCounter + " votes");
		return candidatesMap;
	}

	private List<PartyVotes> getPartyVotes() throws SQLException {
		List<PartyVotes> partyVotesList = new ArrayList<PartyVotes>();
		stmt.execute("SELECT wahlkreis_id, partei_id, stimmen" +
				" FROM zweitstimmen_aggregiert" +
				" ORDER BY wahlkreis_id" +
				LIMIT + ";");
		ResultSet partyResult = stmt.getResultSet();
		int counter = 0, votesCounter = 0;
		while (partyResult.next()) {
			PartyVotes partyVotes = PartyVotes.from(partyResult);
			partyVotesList.add(partyVotes);
			counter++;
			votesCounter += partyVotes.stimmen;
		}
		System.out.println("Found " + counter + " party votes with a total of " + votesCounter + " votes");
		return partyVotesList;
	}

	private void createVotes(final Map<Integer, List<Candidate>> candidatesMap,
							 final List<PartyVotes> partyVotesList) throws SQLException {
		final String INSERT_BASE =
				"INSERT INTO " + insertTableName + "(wahlkreis_id, erst_stimme, zweit_stimme) VALUES ";
		final BulkInsert bulkInsert = new BulkInsert(stmt, INSERT_BASE);
		Candidate currentCandidate = null;
		int votesCounter = 0;
		for (final PartyVotes partyVotes : partyVotesList) {
			while (partyVotes.stimmen > 0) {
				if (currentCandidate == null || currentCandidate.stimmen == 0 ||
						currentCandidate.wahlkreisId != partyVotes.wahlkreisId) {
					currentCandidate = findCandidate(partyVotes, candidatesMap);
				}

				bulkInsert.insert("(" + partyVotes.wahlkreisId +
						"," + (currentCandidate.kandidatId > -1 ? currentCandidate.kandidatId
						: "NULL" /* if no candidate was found */) +
						"," + partyVotes.parteiId + "),");

				currentCandidate.stimmen--;
				partyVotes.stimmen--;
				votesCounter++;
			}
		}
		bulkInsert.finish();
		System.out.println("Created " + votesCounter + " votes");
	}

	private void fillUpCandidateVotes(final Map<Integer, List<Candidate>> candidatesMap) throws SQLException {
		BulkInsert bulkInsert =
				new BulkInsert(stmt,
						"INSERT INTO " + insertTableName + "(wahlkreis_id, erst_stimme, zweit_stimme) VALUES ");
		int counter = 0;
		for (Map.Entry<Integer, List<Candidate>> entry : candidatesMap.entrySet()) {
			List<Candidate> candidates = entry.getValue();
			for (final Candidate candidate : candidates) {
				while (candidate.stimmen > 0) {
					bulkInsert.insert("(" + candidate.wahlkreisId + "," + candidate.kandidatId + ",NULL),");
					candidate.stimmen--;
					counter++;
				}
			}
		}
		System.out.println("Filled up " + counter + " candidate votes");
	}

	private void sanityCheck(final List<PartyVotes> partyVotes,
							 final Map<Integer, List<Candidate>> candidatesMap) {
		for (final PartyVotes partyVote : partyVotes) {
			if (partyVote.stimmen > 0) {
				System.err.println("Party#" + partyVote.parteiId + " has " + partyVote.stimmen + " votes left");
			}
		}

		for (Map.Entry<Integer, List<Candidate>> entry : candidatesMap.entrySet()) {
			List<Candidate> candidates = entry.getValue();
			for (final Candidate candidate : candidates) {
				if (candidate.stimmen > 0) {
					System.err
							.println("Candidate#" + candidate.kandidatId + " has " + candidate.stimmen + " votes left");
				}
			}
		}
	}

	private Candidate findCandidate(final PartyVotes partyVotes,
									final Map<Integer, List<Candidate>> candidatesMap) {
		List<Candidate> candidates = candidatesMap.get(partyVotes.wahlkreisId);
		for (Candidate candidate : candidates) {
			if (candidate.stimmen > 0) return candidate;
		} // return null candidate to generate a first vote not referring to anyone return
		return new Candidate(partyVotes.wahlkreisId, -1, 1);
	}

	private static class Candidate {
		public final int wahlkreisId, kandidatId;
		public int stimmen;

		private Candidate(final int wahlkreisId, final int kandidatId, final int stimmen) {
			this.wahlkreisId = wahlkreisId;
			this.kandidatId = kandidatId;
			this.stimmen = stimmen;
		}

		public static Candidate from(final ResultSet resultSet) throws SQLException {
			return new Candidate(resultSet.getInt("wahlkreis_id"), resultSet.getInt("kandidat_id"),
					resultSet.getInt("stimmen"));
		}
	}

	private static class PartyVotes {
		public final int wahlkreisId, parteiId;
		public int stimmen;

		private PartyVotes(final int wahlkreisId, final int parteiId, final int stimmen) {
			this.wahlkreisId = wahlkreisId;
			this.parteiId = parteiId;
			this.stimmen = stimmen;
		}

		public static PartyVotes from(final ResultSet resultSet) throws SQLException {
			return new PartyVotes(resultSet.getInt("wahlkreis_id"), resultSet.getInt("partei_id"),
					resultSet.getInt("stimmen"));
		}
	}

	private static class BulkInsert {
		private static final int MAX_INSERTS = 50000;

		private final String insertBase;
		private final Statement stmt;

		private StringBuilder statementBuilder;
		private int insertCounter;

		private BulkInsert(final Statement stmt, final String insertBase) {
			this.stmt = stmt;
			this.insertBase = insertBase;

			reset();
		}

		private void reset() {
			statementBuilder = new StringBuilder();
			statementBuilder.append(insertBase);
			insertCounter = 0;
		}

		/**
		 * Requires the values to end with a comma.
		 */
		public void insert(String values) throws SQLException {
			statementBuilder.append(values);
			if (++insertCounter > MAX_INSERTS) {
				executeQuery();
			}
		}

		public void finish() throws SQLException {
			executeQuery();
		}

		private void executeQuery() throws SQLException {
			statementBuilder.deleteCharAt(statementBuilder.length() -1);
			statementBuilder.append(";");

			stmt.execute(statementBuilder.toString());

			reset();
		}
	}
}