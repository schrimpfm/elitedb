/**
 * Created by Patrick on 07.12.2014.
 */
function loading() {
    jQuery(document).ready(function ($) {
        $(document).on('click', '.clickable-row', function () {
            $('div.content').html('<div class="loading" />');
        });
    });
}

${loading}