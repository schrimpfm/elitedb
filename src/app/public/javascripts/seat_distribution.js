function drawSeatDistribution() {
	var data = [];
	$.each(content, function (_, partySeats) {
		data.push({
			name: partySeats.partei,
			y: parseInt(partySeats.sitze),
			color: partySeats.color
		});
	});
	var container = $("#chart");
	container.highcharts({
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: 0,
			plotShadow: false,
			margin: [0, 0, 0, 0],
			spacingTop: 0,
			spacingBottom: 0,
			spacingLeft: 0,
			spacingRight: 0
		},
		title: {
			text: null
		},
		tooltip: {
			headerFormat: '', // no title
			pointFormat: '{point.y} Sitze ({point.percentage:.1f}%)',
			hideDelay: 0
		},
		plotOptions: {
			pie: {
				size: '100%',
				dataLabels: {
					enabled: true,
					distance: -50,
					style: {
						fontWeight: 'bold',
						color: 'white',
						textShadow: '0px 1px 2px black'
					}
				},
				startAngle: -90, // half doughnut
				endAngle: 90,
				center: ['50%', '75%']
			}
		},
		series: [{
			type: 'pie',
			name: 'Sitze',
			innerSize: '50%',
			data: data
		}],
		credits: {
			enabled: false
		}
	});
}

$(drawSeatDistribution);