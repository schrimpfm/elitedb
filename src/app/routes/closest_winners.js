/**
 * Created by Martin on 04.12.2014.
 */
var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');

var parties = undefined;

router.get('/', function (req, res) {
	var year = req.session.year;
	var options = {title: 'Knappste Wahlkreis-Sieger'};
	options['baselink'] = req.baseUrl; //'/knappstesieger';
	var connString = req.app.get('databaseConnectionString');

	var selectedParty = req.query.partei;

	var querystring = "" +
		" SELECT " +
		" sieger_partei || ' - ' || sieger_vorname || ' ' || sieger_nachname as gewinner, " +
		" wahlkreis_name as wahlkreis, " +
		" zweiter_partei || ' - ' || zweiter_vorname || ' ' || zweiter_nachname as zweiter, " +
		" stimmen_differenz as Stimmenabstand " +
		" FROM q6_knappste_sieger_pro_partei" +
		" WHERE jahr = $1";

	querystring += selectedParty == undefined ? "" : " AND sieger_partei = $2";
	var params = selectedParty == undefined ? [year] : [year, selectedParty];

	if (parties == undefined) {
		var getPartyListString = "SELECT name " +
			" FROM partei " +
			" WHERE jahr = $1";

		pgquery.query(connString, getPartyListString, [year])
			.spread(function (err, rows) {
				parties = rows;
				options['selection'] = parties;
			}).then(function () {
				pgquery.query(connString, querystring, params)
					.spread(function (err, rows) {
						renderer.render(err, rows, options, res, 'closest_winners_losers');
					});
			});
	} else {
		options['selection'] = parties;
		pgquery.query(connString, querystring, params)
			.spread(function (err, rows) {
				renderer.render(err, rows, options, res, 'closest_winners_losers');
			});
	}
});

module.exports = router;
