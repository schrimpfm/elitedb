/**
 * Created by Martin on 04.12.2014.
 */
var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');
var election_district = require('../utils/election_districts_base')

var title = 'Wahlkreisübersicht (Einzelstimmen)';

router.get('/', function (req, res) {
    election_district.getBasic(title, req,res);
});

router.get('/:stateName', function (req, res) {
    election_district.getStateName(title, req,res);
});

router.get('/:stateName/:districtNumber', function (req, res) {
    election_district.getDistrictNumber(title, "q3", req,res);
});

module.exports = router;
