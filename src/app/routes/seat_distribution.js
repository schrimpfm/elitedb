/**
 * Created by Martin on 04.12.2014.
 */
var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');

router.get('/', function (req, res) {
	var year = req.session.year;
	var options = {title: 'Sitzverteilung im Bundestag'};
	var querystring = "SELECT s.partei_name AS partei, f.farbe AS color, s.sitze, ROUND(100 * s.sitze / (SELECT SUM(g.sitze) FROM q1_sitzverteilung_im_bundestag g where jahr = $1), 2) || '%' AS anteil" +
		" FROM q1_sitzverteilung_im_bundestag s" +
		" JOIN partei_farbe f ON f.partei_name = s.partei_name" +
		" WHERE jahr = $1" +
		" ORDER BY s.sitze DESC";
	var connString = req.app.get('databaseConnectionString');
	pgquery.query(connString, querystring, [year])
		.spread(function (err, rows) {
			renderer.render(err, rows, options, res, 'seat_distribution');
		});
});

module.exports = router;
