/**
 * Created by Martin on 04.12.2014.
 */
var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');

router.get('/', function (req, res) {
	var year = req.session.year;
	var options = {title: 'Überhangmandate'};
	var querystring = "SELECT *" +
		" FROM q5_ueberhangmandate_pro_partei" +
		" WHERE jahr = $1";
	var connString = req.app.get('databaseConnectionString');
	pgquery.query(connString, querystring, [year])
		.spread(function (err, rows) {
			renderer.render(err, rows, options, res, 'standard_table');
		});
});

module.exports = router;
