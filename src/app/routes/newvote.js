/**
 * Created by Patrick on 18.12.2014.
 */
var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');

var electionYear; // set on first call
var title = 'Stimmabgabe';

router.get('*', function (req, res, next) {
	if (!electionYear) {
		electionYear = req.app.locals.years[0]; // most recent year is the year of the election
	}
	if (req.session.year != electionYear) {
		var options = {title: 'Stimmabgabe ' + req.session.year};
		var error = {message: 'Diese Wahl ist bereits vorbei'};
		renderer.render(error, null, options, res, 'standard_table');
	} else {
		next();
	}
});

router.get('/', function (req, res) {
	var options = {
		title: title,
		subtitle: 'In welchem Bundesland befindet sich Ihr Wahlkreis?',
		path: req.originalUrl
	};
	var queryString = "SELECT name AS id, name AS bundesländer" +
		" FROM bundesland" +
		" WHERE jahr = $1" +
		" ORDER BY name ASC";
	var connString = req.app.get('databaseConnectionStringWaehler');
	pgquery.query(connString, queryString, [electionYear])
		.spread(function (err, rows) {
			renderer.render(err, rows, options, res, 'standard_table');
		});
});

router.get('/:stateName', function (req, res) {
	var stateName = req.param('stateName');
	var options = {
		title: title,
		subtitle: 'Welcher ist Ihr Wahlkreis?',
		path: req.originalUrl
	};
	var queryString = "SELECT id, wahlkreisnummer || ' - ' || name AS wahlkreise" +
		" FROM wahlkreis" +
		" WHERE bundesland_id = (SELECT id FROM bundesland WHERE name = $1 AND jahr = $2)" +
		" ORDER BY id ASC";
	var connString = req.app.get('databaseConnectionStringWaehler');
	pgquery.query(connString, queryString, [stateName, electionYear])
		.spread(function (err, rows) {
			renderer.render(err, rows, options, res, 'standard_table');
		});
});

router.get('/:stateName/:electionDistrictId', function (req, res) {
	var electionDistrictId = req.param('electionDistrictId');
	var options = {
		path: req.originalUrl
	};
	var connString = req.app.get('databaseConnectionStringWaehler');
	pgquery.query(connString, "SELECT name FROM wahlkreis WHERE id = $1", [electionDistrictId])
		.spread(function (err, rows) {
			if (rows && rows.length > 0 && rows[0].hasOwnProperty('name')) {
				options['title'] = title + " für " + rows[0].name;
			} else {
				options['title'] = title;
			}
		}).then(function () {
			var kandidatenQueryString = "" +
				" SELECT k.id, k.id AS nummer, k.vor_name || ' ' || k.nach_name || ' (' || (CASE WHEN p.name IS NULL THEN 'Parteilos' ELSE p.name END) || ')' as kandidat" +
				" FROM wahlkreisliste wl" +
				" JOIN kandidat k on wl.kandidat_id = k.id" +
				" JOIN partei p on k.partei_id = p.id" +
				" WHERE wl.wahlkreis_id = $1" +
				" ORDER BY k.id ASC";

			var parteienQueryString = "" +
				" SELECT p.id, p.name as partei" +
				" FROM wahlkreis w" +
				" JOIN landesliste l on l.bundesland_id = w.bundesland_id" +
				" JOIN partei p on p.id = l.partei_id" +
				" WHERE w.id = $1" +
				" GROUP BY p.id, p.name" +
				" ORDER BY p.name ASC";
			var error = null;
			pgquery.query(connString, kandidatenQueryString, [electionDistrictId])
				.spread(function (err, rows) {
					options['candidates'] = rows;
					error = error || err;
				})
				.then(function () {
					pgquery.query(connString, parteienQueryString, [electionDistrictId])
						.spread(function (err, rows) {
							error = error || err;
							options['parties'] = rows;
							renderer.render(error, null, options, res, 'newvote');
						});
				});
		});
});

function isValid(val) {
	return !(typeof(val) === 'undefined' || val instanceof Array);
}

router.post('/:stateName/:wahlkreisId', function (req, res) {
	var valid = true;
	var wahlkreisId = req.param('wahlkreisId');
	var candidateId = req.param('candidate');
	if (!isValid(candidateId)) {
		candidateId = null;
		valid = false;
	}
	var partyId = req.param('party');
	if (!isValid(partyId)) {
		partyId = null;
		valid = false;
	}
	var keycode = req.param('keycode');
	var connString = req.app.get('databaseConnectionStringWaehler');
	var insertquery = "" +
		" INSERT INTO abgegebene_stimmen (wahlkreis_id, erst_stimme, zweit_stimme, keycode)" +
		" VALUES ($1, $2, $3, $4)";
	pgquery.query(connString, insertquery, [wahlkreisId, candidateId, partyId, keycode])
		.spread(function (err) {
			var options = {
				sent: true,
				valid: valid
			};
			if (err) {
				options['message'] = "Ihre Stimmabgabe konnte nicht gespeichert werden";
			} else {
				options['message'] = "Ihre Stimmabgabe wurde erfolgreich übermittelt";
			}

			renderer.render(err, null, options, res, 'newvote');
		});
});

module.exports = router;
