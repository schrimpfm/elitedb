/**
 * Created by Martin on 04.12.2014.
 */
var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');

var parties = undefined;

router.get('/', function (req, res) {
    var year = req.session.year;
    var options = {title: 'Knappste verlorene Wahlkreise'};
    options['baselink'] = req.baseUrl; //'/knappsteverlierer';
    var connString = req.app.get('databaseConnectionString');

    var selectedParty = req.query.partei;

    var querystring = "" +
        " SELECT" +
        " partei_name || ' - ' || kandidat_vorname || ' ' || kandidat_nachname as verlierer," +
        " sieger_partei || ' - ' || sieger_vorname || ' ' || sieger_nachname as gewinner," +
        " wahlkreis_name as wahlkreis," +
        " fehlende_stimmen" +
        " FROM q6_knappste_verlorene_wahlkreise_pro_partei" +
        " WHERE jahr = $1";

    querystring += selectedParty == undefined ? "" : " AND partei_name = $2";
    var params = selectedParty == undefined ? [year] : [year, selectedParty];

    if (parties == undefined) {
        var getPartyListString = "SELECT name " +
            " FROM partei " +
            " WHERE jahr = $1";

        pgquery.query(connString, getPartyListString, [year])
            .spread(function (err, rows) {
                parties = rows;
                options['selection'] = parties;
            }).then(function () {
                pgquery.query(connString, querystring, params)
                    .spread(function (err, rows) {
                        renderer.render(err, rows, options, res, 'closest_winners_losers');
                    });
            });
    } else {
        options['selection'] = parties;
        pgquery.query(connString, querystring, params)
            .spread(function (err, rows) {
                renderer.render(err, rows, options, res, 'closest_winners_losers');
            });
    }
});

module.exports = router;
