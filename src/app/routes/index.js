var express = require('express');
var router = express.Router();
var pgquery = require('../utils/pgquery');
var renderer = require('../utils/renderer');

/* GET home page. */
router.get('/', function (req, res) {
	var options = {
		'intro': 'Eine Seite zur Demonstration der der wunderschönen deklarativen Sprache SQL auf einer Postgres-Datenbank. Im Auftrag von Florian Funke.<br />' +
		'Direkt ein Query absetzen oder einfach das schöne Design bewundern.'
	};

	var querystring = req.param('querystring', null);
	if (querystring == null) {
		res.render('index', options);
	} else {
		options['querystring'] = querystring;
		var connString = req.app.get('databaseConnectionString');
		pgquery.query(connString, querystring)
			.spread(function (err, rows) {
				renderer.render(err, rows, options, res, 'index');
			});
	}
});

module.exports = router;
