var argumentsCheck = require('../utils/check-arguments');

function getErrorMessage(err) {
	if (err.message != undefined) {
		return (err.name != undefined ? err.name + ": " : "") + err.message;
	} else {
		return err;
	}
}

/**
 * Sets the error and content values of the options object and renders the template with these values.
 * @param err
 * @param content
 * @param options
 * @param response
 * @param template
 */
exports.render = function (err, content, options, response, template) {
	argumentsCheck.validate(5, arguments);

	if (err) {
		options['error'] = getErrorMessage(err);
	} else {
		options['content'] = content;
	}
	response.render(template, options);
};