/**
 * Created by Patrick on 09.01.2014.
 */
var express = require('express');
var pgquery = require('./pgquery');
var renderer = require('./renderer');
var Promise = require('bluebird');

exports.getBasic = function (title, req, res) {
	var year = req.session.year;
	var options = {
		title: title,
		subtitle: 'In welchem Bundesland befindet sich der Wahlkreis?',
		path: req.originalUrl
	};
	var querystring = "SELECT name AS id, name AS bundesländer" +
		" FROM bundesland" +
		" WHERE jahr = $1";
	var connString = req.app.get('databaseConnectionString');
	pgquery.query(connString, querystring, [year])
		.spread(function (err, rows) {
			renderer.render(err, rows, options, res, 'standard_table');
		});
};

exports.getStateName = function (title, req, res) {
	var year = req.session.year,
		stateName = req.param('stateName');
	var options = {
		title: title,
		subtitle: 'Wahlkreise in ' + stateName,
		path: req.originalUrl,
		tableoptions: {
			smalllabel: true,
			td: {
				alignRight: [0]
			}
		}
	};
	var querystring = "SELECT wahlkreisnummer AS id, wahlkreisnummer, name" +
		" FROM wahlkreis" +
		" WHERE bundesland_id = (SELECT id FROM bundesland WHERE jahr = $1 AND name = $2)";
	var connString = req.app.get('databaseConnectionString');
	pgquery.query(connString, querystring, [year, stateName])
		.spread(function (err, rows) {
			renderer.render(err, rows, options, res, 'standard_table');
		});
};

exports.getDistrictNumber = function (title, view, req, res) {
	var year = req.session.year,
		districtNumber = req.param('districtNumber');
	var options = {
		title: title,
		subtitle: districtNumber
	};

	if (view == 'q7') {
		var viewname = 'q7_wahlkreisuebersicht_einzelstimmen_non_materialized';
	} else if (view == 'q3') {
		var viewname = 'q3_wahlkreisuebersicht';
	} else {
		var viewname = view;
	}

	switch (year) {
		case "2009":
			var diff = "'-'";
			break;
		case "2013":
			var diff = "stimmen_diff_13_09 || ' (' || CASE WHEN stimmen_diff_13_09 >= 0 THEN '+' ELSE '' END || ROUND(100 * cast(stimmen_diff_13_09 as decimal(10,2)) / cast(stimmen_absolut_2009 as decimal(10,2)), 2) || '%)'";
			break;
		case "2017":
			var diff = "stimmen_diff_17_13 || ' (' || CASE WHEN stimmen_diff_17_13 >= 0 THEN '+' ELSE '' END || ROUND(100 * cast(stimmen_diff_17_13 as decimal(10,2)) / cast(stimmen_absolut_2013 as decimal(10,2)), 2) || '%)'";
			break;
	}

	//if (year != 2009 && year != 2013) {
	//    options["error"] = "Die Wahl ist noch nicht abgeschlossen.";
	//    res.render('district_info', options);
	//} else
	if (view == 'q7' && (districtNumber < 218 || districtNumber > 220)) {
		options["error"] = "Die Wahlkreisübersicht auf Einzelstimmen funktioniert nur in den Münchner Wahlkreisen 218 - 220.";
		res.render('district_info', options);
	}
	else {
		var querystringDistrict = "SELECT DISTINCT wahlkreisnummer, wahlkreisname, ROUND(wahlbeteiligung_" + year + ", 2) || '%' AS wahlbeteiligung, direktkandidat_" + year + " AS direktkandidat" +
				" FROM " + viewname + " " +
				" WHERE wahlkreisnummer = $1",
			querystringParties = "SELECT partei_name AS partei, stimmen_absolut_" + year + " || ' (' || ROUND(stimmen_relativ_" + year + ", 2) || '%)' AS stimmen, " + diff + " AS veränderung_zum_vorjahr" +
				" FROM " + viewname + " " +
				" WHERE wahlkreisnummer = $1" +
				" ORDER BY stimmen_absolut_" + year + " DESC";

		var connString = req.app.get('databaseConnectionString');
		Promise.join(
			pgquery.query(connString, querystringDistrict, [districtNumber]),
			pgquery.query(connString, querystringParties, [districtNumber]),
			function (districtResult, partiesResult) {
				var err = districtResult[0] || partiesResult[0];
				if (err) {
					options['error'] = err.name + ": " + err.message;
				} else {
					var districtInfo = districtResult[1][0],
						parties = partiesResult[1];
					options['subtitle'] = districtNumber + ' ' + districtInfo.wahlkreisname;
					options['wahlbeteiligung'] = districtInfo.wahlbeteiligung;
					options['direktkandidat'] = districtInfo.direktkandidat;
					options['parteien'] = parties;
				}
			})
			.error(function (err) {
				options['error'] = err.name + ": " + err.message;
			})
			.finally(function () {
				res.render('district_info', options);
			});
	}
};
