exports.validate = function (minimumCount, actualArgs) {
	if (actualArgs.length < minimumCount) {
		throw new Error("expected at least" + minimumCount + " arguments, got " + actualArgs.length);
	}
};

exports.validateMinMax = function (expectedCountMin, expectedCountMax, actualArgs) {
	if (actualArgs.length < expectedCountMin || actualArgs.length > expectedCountMax) {
		throw new Error("expected >=" + expectedCountMin + " and <= " + expectedCountMax + " arguments, got " + actualArgs.length);
	}
};