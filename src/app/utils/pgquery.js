var argumentsCheck = require('../utils/check-arguments');
var Promise = require('bluebird');
var _postgres = require('pg');
var postgres = Promise.promisifyAll(_postgres);

/**
 * @param connectionString the string to connect to the database
 * @param querystring the actual query
 * @param variables an optional array of variables for the query
 * @return an array [error, rows] - either error or rows will be null
 */
exports.query = function (connectionString, querystring, variables) {
	argumentsCheck.validate(2, arguments);

	return postgres.connectAsync(connectionString)
		.spread(function (connection, release) {
			return connection.queryAsync(querystring, variables)
				.then(function render(result) {
					return [null, result.rows];
				})
				.finally(function () {
					release();
				});
		})
		.catch(function (err) {
			return [err, null];
		});
};