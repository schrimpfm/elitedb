var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// use
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ // Populate req.session
	resave: false, // don't save session if unmodified
	saveUninitialized: false, // don't create session until something stored
	secret: 'I <3 Databases'
}));

// constants
app.set('databaseConnectionString', "pg://guest@www.soflu.de:5432/elections");
app.set('databaseConnectionStringWahlleiter', "pg://wahlleiter:!le1tung@www.soflu.de:5432/elections");
app.set('databaseConnectionStringWaehler', "pg://waehler:Waeh1en!@www.soflu.de:5432/elections");

// middleware
app.use(function setLocation(req, res, next) {
	app.locals.pathname = req.originalUrl;
	next(); // keep executing the router middleware
});

function setYear(req, res) {
	var year = req.param('year');
	req.session.year = year;
	res.locals.currentYear = year;
}
app.get('/:year/*', function (req, res, next) {
	setYear(req, res);
	next();
});
app.post('/:year/*', function (req, res, next) {
	setYear(req, res);
	next();
});

// years
app.locals.years = [2017, 2013, 2009];

app.get('/', function (req, res) {
	res.redirect('/' + app.locals.years[1] + '/index');
});

var routes = [
	{
		link: 'index',
		route: 'index',
		dynamicYear: true,
		showInMenu: false
	},
	{
		link: 'sitzverteilung',
		label: 'Q1: Sitzverteilung',
		route: 'seat_distribution',
		dynamicYear: true
	},
	{
		link: 'mdb',
		label: 'Q2: Mitglieder des Bundestages',
		route: 'members_of_parliament',
		dynamicYear: true
	},
	{
		link: 'wahlkreise_aggregiert',
		label: 'Q3: Wahlkreisübersicht (aggregiert)',
		route: 'election_districts_aggregated',
		dynamicYear: true
	},
	{
		link: 'wahlkreissieger',
		label: 'Q4: Wahlkreissieger',
		route: 'election_district_winners',
		dynamicYear: true
	},
	{
		link: 'ueberhangmandate',
		label: 'Q5: Überhangmandate',
		route: 'overhang_mandates',
		dynamicYear: true
	},
	{
		link: 'knappstesieger',
		label: 'Q6: Knappste Sieger',
		route: 'closest_winners',
		dynamicYear: true
	},
	{
		link: 'knappsteverlierer',
		label: 'Q6: Knappste Verlierer',
		route: 'closest_losers',
		dynamicYear: true
	},
	{
		link: 'wahlkreise_einzelstimmen',
		label: 'Q7: Wahlkreisübersicht (Einzelstimmen)',
		route: 'election_districts_votes',
		dynamicYear: true
	},
	{divider: true},
	{
		link: 'stimmabgabe',
		label: 'Stimmabgabe',
		route: 'newvote',
		dynamicYear: true
	}
];

app.locals.menu_links = routes;

for (var r = 0; r < routes.length; r++) {
	var routeObj = routes[r];
	if (routeObj.route) {
		var fn = require('./routes/' + routeObj.route);
		if (routeObj.dynamicYear) {
			for (var y = 0; y < app.locals.years.length; y++) {
				var year = app.locals.years[y];
				app.use('/' + year + '/' + routeObj.link, fn);
			}
		} else {
			app.use('/' + routeObj.link, fn);
		}
	}
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

/* error handlers */

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


app.use(express.static(__dirname + '/public'));

/* export */
module.exports = app;
