-- Gast
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM guest;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM guest;
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM guest;
REVOKE ALL PRIVILEGES ON DATABASE elections FROM guest;
DROP ROLE guest;

CREATE ROLE guest WITH LOGIN;

GRANT CONNECT ON DATABASE elections TO guest;

GRANT SELECT ON q1_sitzverteilung_im_bundestag TO guest;
GRANT SELECT ON q2_mitglieder_des_bundestages TO guest;
GRANT SELECT ON q4_wahlkreissieger TO guest;
GRANT SELECT ON q3_wahlkreisuebersicht TO guest;
GRANT SELECT ON q5_ueberhangmandate_pro_partei TO guest;
GRANT SELECT ON q6_knappste_verlorene_wahlkreise_pro_partei TO guest;
GRANT SELECT ON q6_knappste_sieger_pro_partei TO guest;
GRANT SELECT ON q7_wahlkreisuebersicht_einzelstimmen TO guest;
GRANT SELECT ON q7_wahlkreisuebersicht_einzelstimmen_non_materialized TO guest;

GRANT SELECT ON bundesland TO guest;
GRANT SELECT ON wahljahr TO guest;
GRANT SELECT ON wahlkreis TO guest;
GRANT SELECT ON partei TO guest;
GRANT SELECT ON partei_farbe TO guest;
GRANT SELECT ON kandidat TO guest;
GRANT SELECT ON wahlkreisliste TO guest;
GRANT SELECT ON wahlkreis_kandidaten_ergebnis TO guest;
GRANT SELECT ON wahlkreis_partei_ergebnis TO guest;
GRANT SELECT ON erststimmen_aggregiert TO guest;
GRANT SELECT ON zweitstimmen_aggregiert TO guest;
GRANT SELECT ON gesamtsitze_pro_partei_und_bundesland_2009 TO guest; -- mehr Infos als in q1-View, z.B. Unterscheidung zwischen Bundesländern
GRANT SELECT ON gesamtsitze_pro_partei_und_bundesland_2013 TO guest;
GRANT SELECT ON gesamtsitze_pro_partei_und_bundesland_2017 TO guest;
GRANT SELECT ON mitglieder_des_bundestags_2009 TO guest; -- ids statt Namen wie in q2
GRANT SELECT ON mitglieder_des_bundestags_2013 TO guest;
GRANT SELECT ON mitglieder_des_bundestags_2017 TO guest;
GRANT SELECT ON sitze_pro_partei_und_bl TO guest;
GRANT SELECT ON ueberhangmandate_pro_partei_2009 TO guest;
GRANT SELECT ON ueberhangmandate_pro_partei_2013 TO guest;
GRANT SELECT ON ueberhangmandate_pro_partei_2017 TO guest;
GRANT SELECT ON gesamtsitze_pro_partei_2009 TO guest;
GRANT SELECT ON gesamtsitze_pro_partei_2013 TO guest;
GRANT SELECT ON gesamtsitze_pro_partei_2017 TO guest;


-- Waehler
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM waehler;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM waehler;
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM waehler;
REVOKE ALL PRIVILEGES ON DATABASE elections FROM waehler;
DROP ROLE waehler;

CREATE ROLE waehler WITH LOGIN PASSWORD 'Waeh1en!';

GRANT CONNECT ON DATABASE elections TO waehler;

GRANT INSERT ON abgegebene_stimmen TO waehler;
GRANT USAGE ON abgegebene_stimmen_id_seq TO waehler;

GRANT SELECT ON wahlkreisliste TO waehler;
GRANT SELECT ON landesliste TO waehler;
GRANT SELECT ON kandidat TO waehler;
GRANT SELECT ON partei TO waehler;
GRANT SELECT ON bundesland TO waehler;
GRANT SELECT ON wahlkreis TO waehler;


-- Wahlleiter
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM wahlleiter;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM wahlleiter;
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM wahlleiter;
REVOKE ALL PRIVILEGES ON DATABASE elections FROM wahlleiter;
DROP ROLE wahlleiter;

CREATE ROLE wahlleiter WITH LOGIN PASSWORD '!le1tung';

GRANT CONNECT ON DATABASE elections TO wahlleiter;

GRANT INSERT, UPDATE, DELETE ON bundesland TO wahlleiter; -- DELETE in Ordnung, weil durch die Integritätsbedingungen sichergestellt wird, dass nichts gelöscht wird, wenn Daten (z.B. Stimmen) darauf zeigen
GRANT INSERT, UPDATE, DELETE ON wahlkreis TO wahlleiter;
GRANT INSERT, UPDATE, DELETE ON partei TO wahlleiter;
GRANT INSERT, UPDATE, DELETE ON partei_farbe TO wahlleiter;
GRANT INSERT, UPDATE, DELETE ON kandidat TO wahlleiter;
GRANT INSERT, UPDATE, DELETE ON wahlkreisliste TO wahlleiter;
GRANT INSERT, UPDATE, DELETE ON landesliste TO wahlleiter;
GRANT UPDATE ON konstanten TO wahlleiter;

GRANT EXECUTE ON FUNCTION getKeycode(INTEGER) TO wahlleiter; -- Wahlleiter kann für Wähler und sich selber Keycode generieren
GRANT INSERT ON abgegebene_stimmen TO wahlleiter; -- Wahlleiter darf funktionalen Anforderungen zufolge für andere Stimmen eintragen
GRANT USAGE ON abgegebene_stimmen_id_seq TO wahlleiter;
GRANT USAGE ON bundesland_id_seq TO wahlleiter;
GRANT USAGE ON wahlkreis_id_seq TO wahlleiter;


-- Developers
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO developers;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA information_schema TO developers;