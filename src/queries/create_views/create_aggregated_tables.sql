CREATE TABLE wahlkreis_partei_ergebnis_table
(
  wahlkreis_id INTEGER,
  partei_id    INTEGER,
  stimmen      INTEGER,
  PRIMARY KEY (wahlkreis_id, partei_id),
  FOREIGN KEY (wahlkreis_id) REFERENCES wahlkreis (id),
  FOREIGN KEY (partei_id) REFERENCES partei (id)
);

CREATE TABLE wahlkreis_kandidaten_ergebnis_table (
  wahlkreis_id INTEGER,
  kandidat_id  INTEGER,
  stimmen      INTEGER,
  PRIMARY KEY (wahlkreis_id, kandidat_id),
  FOREIGN KEY (wahlkreis_id) REFERENCES wahlkreis (id),
  FOREIGN KEY (kandidat_id) REFERENCES kandidat (id)
);

CREATE OR REPLACE FUNCTION refresh_wahlkreis_partei_ergebnis_table()
  RETURNS BOOL
AS $refresh_wahlkreis_partei_ergebnis_table$
BEGIN
  TRUNCATE TABLE wahlkreis_partei_ergebnis_table;
  INSERT INTO wahlkreis_partei_ergebnis_table
    SELECT
      wz.wahlkreis_id,
      wz.zweit_stimme,
      count(*)
    FROM wahlzettel wz
    WHERE zweit_stimme IS NOT NULL
    GROUP BY wz.wahlkreis_id, wz.zweit_stimme;
  RETURN TRUE;
END;
$refresh_wahlkreis_partei_ergebnis_table$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION refresh_wahlkreis_kandidaten_ergebnis_table()
  RETURNS BOOL
AS $refresh_wahlkreis_kandidaten_ergebnis_table$
BEGIN
  TRUNCATE TABLE wahlkreis_kandidaten_ergebnis_table;
  INSERT INTO wahlkreis_kandidaten_ergebnis_table
    SELECT
      wz.wahlkreis_id,
      wz.erst_stimme,
      count(*)
    FROM Wahlzettel wz
    WHERE erst_stimme IS NOT NULL
    GROUP BY wz.wahlkreis_id, wz.erst_stimme;
  RETURN TRUE;
END;
$refresh_wahlkreis_kandidaten_ergebnis_table$ LANGUAGE plpgsql;