CREATE OR REPLACE VIEW q4_wahlkreissieger(wahlkreisnummer, wahlkreisname, kandidat_vorname, kandidat_nachname, jahr) AS
  (
    WITH max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen, jahr) AS
    (
        SELECT
          wke.wahlkreis_id,
          max(wke.stimmen),
          jahr
        FROM "wahlkreis_kandidaten_ergebnis_table" wke
          JOIN wahlkreis w ON wke.wahlkreis_id = w.id
        GROUP BY wahlkreis_id, jahr
    )
    SELECT
      w.wahlkreisnummer,
      w.name,
      k.vor_name,
      k.nach_name,
      mspw.jahr
    FROM "wahlkreis_kandidaten_ergebnis_table" wke
      JOIN max_stimmen_pro_wahlkreis mspw ON wke.wahlkreis_id = mspw.wahlkreis_id
      JOIN wahlkreis w ON wke.wahlkreis_id = w.id
      JOIN kandidat k ON wke.kandidat_id = k.id
    WHERE stimmen = mspw.max_stimmen
    ORDER BY mspw.jahr DESC
  );