CREATE OR REPLACE VIEW q6_knappste_verlorene_wahlkreise_pro_partei(
    partei_name,
    wahlkreis_name,
    kandidat_vorname,
    kandidat_nachname,
    fehlende_stimmen,
    sieger_vorname,
    sieger_nachname,
    sieger_partei,
    jahr) AS
  (
    WITH max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen, jahr) AS
    (
        SELECT
          wke.wahlkreis_id,
          max(wke.stimmen),
          w.jahr
        FROM wahlkreis_kandidaten_ergebnis_table wke
          JOIN wahlkreis w ON w.id = wke.wahlkreis_id
        GROUP BY wahlkreis_id, jahr
    ),
        wahlkreis_sieger(wahlkreis_id, kandidat_id, partei_id, stimmen, jahr) AS
      (
          SELECT
            wke.wahlkreis_id,
            wke.kandidat_id,
            k.partei_id,
            wke.stimmen,
            mspw.jahr
          FROM wahlkreis_kandidaten_ergebnis_table wke
            JOIN max_stimmen_pro_wahlkreis mspw ON wke.wahlkreis_id = mspw.wahlkreis_id
            JOIN kandidat k ON wke.kandidat_id = k.id
          WHERE stimmen = mspw.max_stimmen
      ),
        parteien_ohne_gewonnene_wahlkreise (partei_id, jahr) AS
      (
          SELECT
            p.id,
            p.jahr
          FROM partei p
            LEFT OUTER JOIN wahlkreis_sieger ws ON ws.partei_id = p.id AND ws.jahr = p.jahr
          WHERE ws.jahr IS NULL
      )
    SELECT
      partei_name,
      wahlkreis_name,
      kandidat_vorname,
      kandidat_nachname,
      fehlende_stimmen,
      sieger_vorname,
      sieger_nachname,
      sieger_partei,
      jahr
    FROM
      (SELECT
         p.name                                     AS partei_name,
         w.name                                     AS wahlkreis_name,
         k.vor_name                                 AS kandidat_vorname,
         k.nach_name                                AS kandidat_nachname,
         ws.stimmen - wke.stimmen                   AS fehlende_stimmen,
         sk.vor_name                                AS sieger_vorname,
         sk.nach_name                               AS sieger_nachname,
         sp.name                                    AS sieger_partei,
         rank()
         OVER (PARTITION BY p.id, p.jahr
           ORDER BY (ws.stimmen - wke.stimmen) ASC) AS knappheitsgrad,
         p.jahr
       FROM wahlkreis_kandidaten_ergebnis_table wke
         JOIN kandidat k ON wke.kandidat_id = k.id
         JOIN parteien_ohne_gewonnene_wahlkreise pogw ON k.partei_id = pogw.partei_id
         JOIN wahlkreis_sieger ws ON wke.wahlkreis_id = ws.wahlkreis_id
         JOIN partei sp ON ws.partei_id = sp.id
         JOIN kandidat sk ON ws.kandidat_id = sk.id
         JOIN wahlkreis w ON wke.wahlkreis_id = w.id
         JOIN partei p ON pogw.partei_id = p.id) wvus
    WHERE knappheitsgrad <= 10
  );
