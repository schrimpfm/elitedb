﻿create view gesamtsitze_pro_partei(partei_id, sitze) as
(
	select partei_id, sum(sitze)
	from gesamtsitze_pro_partei_und_bundesland_2013
	group by partei_id
);