DROP MATERIALIZED VIEW mitglieder_des_bundestags_2009;

CREATE MATERIALIZED VIEW mitglieder_des_bundestags_2009 AS
  (
    WITH direktmandate(kandidat_id, wahlkreis_id) AS
    (
        SELECT
          w.kandidat_id,
          w.wahlkreis_id
        FROM wahlkreis_kandidaten_ergebnis_table w
          JOIN wahlkreis wk ON w.wahlkreis_id = wk.id
        WHERE wk.jahr = 2009
              AND stimmen = (SELECT max(stimmen)
                             FROM wahlkreis_kandidaten_ergebnis_table w2
                             WHERE w2.wahlkreis_id = w.wahlkreis_id)
    ),
        direktmandate_pro_partei_und_bundesland(partei_id, bundesland_id, direktmandate) AS
      (
          SELECT
            k.partei_id,
            w.bundesland_id,
            count(*)
          FROM direktmandate d
            JOIN kandidat k ON k.id = d.kandidat_id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE w.jahr = 2009 AND k.partei_id IS NOT NULL
          GROUP BY k.partei_id, w.bundesland_id
      ),
        landeslisten_ohne_direktmandate(bundesland_id, partei_id, prio, kandidat_id, prio_ohne_direktkandidaten) AS
      (
          SELECT
            l.*,
            rank()
            OVER (PARTITION BY bundesland_id, partei_id
              ORDER BY prio ASC) AS prio_ohne_direktkandidaten
          FROM landesliste l
            JOIN bundesland b ON b.id = l.bundesland_id
          WHERE b.jahr = 2009
                AND kandidat_id
                    NOT IN (SELECT kandidat_id
                            FROM direktmandate)
      )
    SELECT
      k.id                          AS kandidat_id,
      k.partei_id,
      dk.bundesland_id,
      dk.prio_ohne_direktkandidaten AS listenplatz
    FROM kandidat k
      JOIN
      (SELECT
         lod.kandidat_id,
         lod.bundesland_id,
         lod.prio_ohne_direktkandidaten
       FROM gesamtsitze_pro_partei_und_bundesland_2009 gppubl
         JOIN landeslisten_ohne_direktmandate lod
           ON gppubl.partei_id = lod.partei_id AND gppubl.bundesland_id = lod.bundesland_id
         LEFT OUTER JOIN direktmandate_pro_partei_und_bundesland dppub ON gppubl.bundesland_id = dppub.bundesland_id AND dppub.partei_id = gppubl.partei_id
       WHERE lod.prio_ohne_direktkandidaten <= (gppubl.sitze - coalesce(dppub.direktmandate, 0))
       UNION
       SELECT
         kandidat_id,
         w.bundesland_id AS bundesland_id,
         NULL            AS prio_ohne_direktkandidaten
       FROM direktmandate d
         JOIN wahlkreis w ON d.wahlkreis_id = w.id) dk ON dk.kandidat_id = k.id
    ORDER BY k.partei_id, dk.bundesland_id, dk.prio_ohne_direktkandidaten
  );