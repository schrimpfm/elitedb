DROP MATERIALIZED VIEW q7_wahlkreisuebersicht_einzelstimmen;
CREATE MATERIALIZED VIEW q7_wahlkreisuebersicht_einzelstimmen(
    wahlkreisnummer,
    wahlkreisname,
    wahlbeteiligung_2013,
    wahlbeteiligung_2009,
    wahlbeteiligung_2017,
    wahlbeteiligung_diff_13_09,
    wahlbeteiligung_diff_17_13,
    direktkandidat_2013,
    direktkandidat_2009,
    direktkandidat_2017,
    partei_name,
    stimmen_absolut_2013,
    stimmen_absolut_2009,
    stimmen_absolut_2017,
    stimmen_relativ_2013,
    stimmen_relativ_2009,
    stimmen_relativ_2017,
    stimmen_diff_13_09,
    stimmen_diff_17_13) AS
  (
    WITH interessante_wahlkreise (wahlkreis_id) AS
    (
        SELECT *
        FROM (VALUES (218), (219), (220), (221), (222), (517), (518), (519), (520), (521), (7994), (7995), (7996), (7997), (7998), (7999)) AS wahlkreise(id)
    ),
        interessante_wahlzettel(id, wahlkreis_id, erst_stimme, zweit_stimme) AS
      (
          SELECT *
          FROM wahlzettel
          WHERE wahlkreis_id IN (SELECT *
                                 FROM interessante_wahlkreise)
      ),
        zweitstimmen_pro_wahlkreis(wahlkreis_id, zweitstimmen) AS
      (
          SELECT
            wahlkreis_id,
            count(*)
          FROM interessante_wahlzettel
          GROUP BY wahlkreis_id
      ),
        wahlbeteiligung_pro_wahlkreis (wahlkreis_id, wahlbeteiligung) AS
      (
          SELECT
            w.id AS wahlkreis_id,
            round(zpw.zweitstimmen / CAST(w.wahlberechtigte AS DECIMAL), 4) * 100
          FROM wahlkreis w
            JOIN zweitstimmen_pro_wahlkreis zpw ON w.id = zpw.wahlkreis_id
      ),
        stimmen_pro_wahlkreis_und_kandidat(wahlkreis_id, kandidat_id, stimmen) AS
      (
          SELECT
            wahlkreis_id,
            erst_stimme,
            count(*) AS stimmen
          FROM interessante_wahlzettel
          GROUP BY wahlkreis_id, erst_stimme
      ),
        max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen) AS
      (
          SELECT
            wahlkreis_id,
            max(stimmen)
          FROM stimmen_pro_wahlkreis_und_kandidat
          GROUP BY wahlkreis_id
      ),
        direktkandidat_pro_wahlkreis(wahlkreis_id, kandidat_id) AS
      (SELECT
         spwuk.wahlkreis_id,
         spwuk.kandidat_id
       FROM stimmen_pro_wahlkreis_und_kandidat spwuk
         JOIN max_stimmen_pro_wahlkreis mspw ON spwuk.wahlkreis_id = mspw.wahlkreis_id
       WHERE spwuk.stimmen = mspw.max_stimmen),
        stimmen_pro_wahlkreis_und_partei(wahlkreis_id, partei_id, stimmen_absolut, stimmen_relativ) AS
      (
          SELECT
            spwup.wahlkreis_id,
            spwup.zweit_stimme,
            spwup.stimmen,
            round(spwup.stimmen / CAST(zpw.zweitstimmen AS DECIMAL), 4) * 100
          FROM (
                 SELECT
                   wahlkreis_id,
                   zweit_stimme,
                   count(*) AS stimmen
                 FROM interessante_wahlzettel
                 GROUP BY wahlkreis_id, zweit_stimme) spwup
            JOIN zweitstimmen_pro_wahlkreis zpw ON zpw.wahlkreis_id = spwup.wahlkreis_id
      ),
        wahlkreis_daten(wahlkreis_id, wahlbeteiligung, kandidat_id) AS
      (SELECT
         wpw.wahlkreis_id,
         wpw.wahlbeteiligung,
         dpw.kandidat_id
       FROM wahlbeteiligung_pro_wahlkreis wpw
         JOIN direktkandidat_pro_wahlkreis dpw ON wpw.wahlkreis_id = dpw.wahlkreis_id
      ),
        wahlkreise_ohne_umbenennung (wahlkreis_id_2013, wahlkreis_id_2009, wahlkreis_id_2017) AS
      (SELECT
         w13.id,
         w9.id,
         w17.id
       FROM wahlkreis w9
         JOIN wahlkreis w13 ON w9.name LIKE w13.name
         JOIN wahlkreis w17 ON w9.name LIKE w17.name
       WHERE w9.jahr = 2009 AND w13.jahr = 2013 AND w17.jahr = 2017),
        wahlkreis_vergleich(wahlkreis_id_2013, wahlkreis_id_2009, wahlkreis_id_2017, wahlbeteiligung_2013, wahlbeteiligung_2009, wahlbeteiligung_2017, wahlbeteiligung_diff_13_09, wahlbeteiligung_diff_17_13, direktkandidat_2013, direktkandidat_2009, direktkandidat_2017) AS
      (
          SELECT
            wou.wahlkreis_id_2013,
            wou.wahlkreis_id_2009,
            wou.wahlkreis_id_2017,
            wd_13.wahlbeteiligung,
            wd_09.wahlbeteiligung,
            wd_17.wahlbeteiligung,
            round((wd_13.wahlbeteiligung - wd_09.wahlbeteiligung) / wd_09.wahlbeteiligung, 4) * 100,
            round((wd_17.wahlbeteiligung - wd_13.wahlbeteiligung) / wd_13.wahlbeteiligung, 4) * 100,
            wd_13.kandidat_id,
            wd_09.kandidat_id,
            wd_17.kandidat_id
          FROM wahlkreise_ohne_umbenennung wou
            JOIN wahlkreis_daten wd_13 ON wou.wahlkreis_id_2013 = wd_13.wahlkreis_id
            JOIN wahlkreis_daten wd_09 ON wou.wahlkreis_id_2009 = wd_09.wahlkreis_id
            LEFT OUTER JOIN wahlkreis_daten wd_17 ON wou.wahlkreis_id_2017 = wd_17.wahlkreis_id
      ),
        wahlkreis_partei_vergleich(wahlkreis_id_2013, wahlkreis_id_2009, wahlkreis_id_2017, partei_id, stimmen_absolut_2013, stimmen_absolut_2009, stimmen_absolut_2017, stimmen_relativ_2013, stimmen_relativ_2009, stimmen_relativ_2017, stimmen_diff_13_09, stimmen_diff_17_13) AS
      (
          SELECT
            wou.wahlkreis_id_2013,
            wou.wahlkreis_id_2009,
            wou.wahlkreis_id_2017,
            spwup_13.partei_id,
            spwup_13.stimmen_absolut,
            spwup_09.stimmen_absolut,
            spwup_17.stimmen_absolut,
            spwup_13.stimmen_relativ,
            spwup_09.stimmen_relativ,
            spwup_17.stimmen_relativ,
            spwup_13.stimmen_absolut - spwup_09.stimmen_absolut,
            spwup_17.stimmen_absolut - spwup_13.stimmen_absolut
          FROM wahlkreise_ohne_umbenennung wou
            JOIN stimmen_pro_wahlkreis_und_partei spwup_13 ON wou.wahlkreis_id_2013 = spwup_13.wahlkreis_id
            JOIN partei p13 ON spwup_13.partei_id = p13.id
            JOIN stimmen_pro_wahlkreis_und_partei spwup_09 ON wou.wahlkreis_id_2009 = spwup_09.wahlkreis_id
            JOIN partei p9 ON spwup_09.partei_id = p9.id
            LEFT OUTER JOIN stimmen_pro_wahlkreis_und_partei spwup_17 ON wou.wahlkreis_id_2017 = spwup_17.wahlkreis_id
            LEFT OUTER JOIN partei p17 ON spwup_17.partei_id = p17.id
          WHERE p13.name LIKE p9.name AND (p17.name ISNULL OR p13.name LIKE p17.name)
      )
    SELECT
      w.wahlkreisnummer,
      w.name,
      wv.wahlbeteiligung_2013,
      wv.wahlbeteiligung_2009,
      wv.wahlbeteiligung_2017,
      wv.wahlbeteiligung_diff_13_09,
      wv.wahlbeteiligung_diff_17_13,
      k_2013.vor_name || ' ' || k_2013.nach_name,
      k_2009.vor_name || ' ' || k_2009.nach_name,
      k_2017.vor_name || ' ' || k_2017.nach_name,
      p.name,
      wpv.stimmen_absolut_2013,
      wpv.stimmen_absolut_2009,
      wpv.stimmen_absolut_2017,
      wpv.stimmen_relativ_2013,
      wpv.stimmen_relativ_2009,
      wpv.stimmen_relativ_2017,
      wpv.stimmen_diff_13_09,
      wpv.stimmen_diff_17_13
    FROM wahlkreis_vergleich wv
      JOIN wahlkreis_partei_vergleich wpv
        ON wv.wahlkreis_id_2013 = wpv.wahlkreis_id_2013 AND wv.wahlkreis_id_2009 = wpv.wahlkreis_id_2009
      JOIN wahlkreis w ON w.id = wpv.wahlkreis_id_2013
      JOIN partei p ON wpv.partei_id = p.id
      JOIN kandidat k_2013 ON wv.direktkandidat_2013 = k_2013.id
      JOIN kandidat k_2009 ON wv.direktkandidat_2009 = k_2009.id
      LEFT OUTER JOIN kandidat k_2017 ON wv.direktkandidat_2017 = k_2017.id
    ORDER BY wv.wahlkreis_id_2013, wpv.partei_id
  );

CREATE OR REPLACE VIEW q7_wahlkreisuebersicht_einzelstimmen_non_materialized(
    wahlkreisnummer,
    wahlkreisname,
    wahlbeteiligung_2013,
    wahlbeteiligung_2009,
    wahlbeteiligung_2017,
    wahlbeteiligung_diff_13_09,
    wahlbeteiligung_diff_17_13,
    direktkandidat_2013,
    direktkandidat_2009,
    direktkandidat_2017,
    partei_name,
    stimmen_absolut_2013,
    stimmen_absolut_2009,
    stimmen_absolut_2017,
    stimmen_relativ_2013,
    stimmen_relativ_2009,
    stimmen_relativ_2017,
    stimmen_diff_13_09,
    stimmen_diff_17_13) AS
  (
    WITH interessante_wahlkreise (wahlkreis_id) AS
    (
        SELECT *
        FROM (VALUES (218), (219), (220), (221), (222), (517), (518), (519), (520), (521), (7994), (7995), (7996), (7997), (7998), (7999)) AS wahlkreise(id)
    ),
        interessante_wahlzettel(id, wahlkreis_id, erst_stimme, zweit_stimme) AS
      (
          SELECT *
          FROM wahlzettel
          WHERE wahlkreis_id IN (SELECT *
                                 FROM interessante_wahlkreise)
      ),
        zweitstimmen_pro_wahlkreis(wahlkreis_id, zweitstimmen) AS
      (
          SELECT
            wahlkreis_id,
            count(*)
          FROM interessante_wahlzettel
          GROUP BY wahlkreis_id
      ),
        wahlbeteiligung_pro_wahlkreis (wahlkreis_id, wahlbeteiligung) AS
      (
          SELECT
            w.id AS wahlkreis_id,
            round(zpw.zweitstimmen / CAST(w.wahlberechtigte AS DECIMAL), 4) * 100
          FROM wahlkreis w
            JOIN zweitstimmen_pro_wahlkreis zpw ON w.id = zpw.wahlkreis_id
      ),
        stimmen_pro_wahlkreis_und_kandidat(wahlkreis_id, kandidat_id, stimmen) AS
      (
          SELECT
            wahlkreis_id,
            erst_stimme,
            count(*) AS stimmen
          FROM interessante_wahlzettel
          GROUP BY wahlkreis_id, erst_stimme
      ),
        max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen) AS
      (
          SELECT
            wahlkreis_id,
            max(stimmen)
          FROM stimmen_pro_wahlkreis_und_kandidat
          GROUP BY wahlkreis_id
      ),
        direktkandidat_pro_wahlkreis(wahlkreis_id, kandidat_id) AS
      (SELECT
         spwuk.wahlkreis_id,
         spwuk.kandidat_id
       FROM stimmen_pro_wahlkreis_und_kandidat spwuk
         JOIN max_stimmen_pro_wahlkreis mspw ON spwuk.wahlkreis_id = mspw.wahlkreis_id
       WHERE spwuk.stimmen = mspw.max_stimmen),
        stimmen_pro_wahlkreis_und_partei(wahlkreis_id, partei_id, stimmen_absolut, stimmen_relativ) AS
      (
          SELECT
            spwup.wahlkreis_id,
            spwup.zweit_stimme,
            spwup.stimmen,
            round(spwup.stimmen / CAST(zpw.zweitstimmen AS DECIMAL), 4) * 100
          FROM (
                 SELECT
                   wahlkreis_id,
                   zweit_stimme,
                   count(*) AS stimmen
                 FROM interessante_wahlzettel
                 GROUP BY wahlkreis_id, zweit_stimme) spwup
            JOIN zweitstimmen_pro_wahlkreis zpw ON zpw.wahlkreis_id = spwup.wahlkreis_id
      ),
        wahlkreis_daten(wahlkreis_id, wahlbeteiligung, kandidat_id) AS
      (SELECT
         wpw.wahlkreis_id,
         wpw.wahlbeteiligung,
         dpw.kandidat_id
       FROM wahlbeteiligung_pro_wahlkreis wpw
         JOIN direktkandidat_pro_wahlkreis dpw ON wpw.wahlkreis_id = dpw.wahlkreis_id
      ),
        wahlkreise_ohne_umbenennung (wahlkreis_id_2013, wahlkreis_id_2009, wahlkreis_id_2017) AS
      (SELECT
         w13.id,
         w9.id,
         w17.id
       FROM wahlkreis w9
         JOIN wahlkreis w13 ON w9.name LIKE w13.name
         JOIN wahlkreis w17 ON w9.name LIKE w17.name
       WHERE w9.jahr = 2009 AND w13.jahr = 2013 AND w17.jahr = 2017),
        wahlkreis_vergleich(wahlkreis_id_2013, wahlkreis_id_2009, wahlkreis_id_2017, wahlbeteiligung_2013, wahlbeteiligung_2009, wahlbeteiligung_2017, wahlbeteiligung_diff_13_09, wahlbeteiligung_diff_17_13, direktkandidat_2013, direktkandidat_2009, direktkandidat_2017) AS
      (
          SELECT
            wou.wahlkreis_id_2013,
            wou.wahlkreis_id_2009,
            wou.wahlkreis_id_2017,
            wd_13.wahlbeteiligung,
            wd_09.wahlbeteiligung,
            wd_17.wahlbeteiligung,
            round((wd_13.wahlbeteiligung - wd_09.wahlbeteiligung) / wd_09.wahlbeteiligung, 4) * 100,
            round((wd_17.wahlbeteiligung - wd_13.wahlbeteiligung) / wd_13.wahlbeteiligung, 4) * 100,
            wd_13.kandidat_id,
            wd_09.kandidat_id,
            wd_17.kandidat_id
          FROM wahlkreise_ohne_umbenennung wou
            JOIN wahlkreis_daten wd_13 ON wou.wahlkreis_id_2013 = wd_13.wahlkreis_id
            JOIN wahlkreis_daten wd_09 ON wou.wahlkreis_id_2009 = wd_09.wahlkreis_id
            LEFT OUTER JOIN wahlkreis_daten wd_17 ON wou.wahlkreis_id_2017 = wd_17.wahlkreis_id
      ),
        wahlkreis_partei_vergleich(wahlkreis_id_2013, wahlkreis_id_2009, wahlkreis_id_2017, partei_id, stimmen_absolut_2013, stimmen_absolut_2009, stimmen_absolut_2017, stimmen_relativ_2013, stimmen_relativ_2009, stimmen_relativ_2017, stimmen_diff_13_09, stimmen_diff_17_13) AS
      (
          SELECT
            wou.wahlkreis_id_2013,
            wou.wahlkreis_id_2009,
            wou.wahlkreis_id_2017,
            spwup_13.partei_id,
            spwup_13.stimmen_absolut,
            spwup_09.stimmen_absolut,
            spwup_17.stimmen_absolut,
            spwup_13.stimmen_relativ,
            spwup_09.stimmen_relativ,
            spwup_17.stimmen_relativ,
            spwup_13.stimmen_absolut - spwup_09.stimmen_absolut,
            spwup_17.stimmen_absolut - spwup_13.stimmen_absolut
          FROM wahlkreise_ohne_umbenennung wou
            JOIN stimmen_pro_wahlkreis_und_partei spwup_13 ON wou.wahlkreis_id_2013 = spwup_13.wahlkreis_id
            LEFT OUTER JOIN partei p13 ON spwup_13.partei_id = p13.id
            JOIN stimmen_pro_wahlkreis_und_partei spwup_09 ON wou.wahlkreis_id_2009 = spwup_09.wahlkreis_id
            LEFT OUTER JOIN partei p9 ON spwup_09.partei_id = p9.id
            LEFT OUTER JOIN stimmen_pro_wahlkreis_und_partei spwup_17 ON wou.wahlkreis_id_2017 = spwup_17.wahlkreis_id
            LEFT OUTER JOIN partei p17 ON spwup_17.partei_id = p17.id
          WHERE p13.name LIKE p9.name AND (p17.name ISNULL OR p13.name LIKE p17.name)
      )
    SELECT
      w.wahlkreisnummer,
      w.name,
      wv.wahlbeteiligung_2013,
      wv.wahlbeteiligung_2009,
      wv.wahlbeteiligung_2017,
      wv.wahlbeteiligung_diff_13_09,
      wv.wahlbeteiligung_diff_17_13,
      k_2013.vor_name || ' ' || k_2013.nach_name,
      k_2009.vor_name || ' ' || k_2009.nach_name,
      k_2017.vor_name || ' ' || k_2017.nach_name,
      p.name,
      wpv.stimmen_absolut_2013,
      wpv.stimmen_absolut_2009,
      wpv.stimmen_absolut_2017,
      wpv.stimmen_relativ_2013,
      wpv.stimmen_relativ_2009,
      wpv.stimmen_relativ_2017,
      wpv.stimmen_diff_13_09,
      wpv.stimmen_diff_17_13
    FROM wahlkreis_vergleich wv
      JOIN wahlkreis_partei_vergleich wpv
        ON wv.wahlkreis_id_2013 = wpv.wahlkreis_id_2013 AND wv.wahlkreis_id_2009 = wpv.wahlkreis_id_2009
      JOIN wahlkreis w ON w.id = wpv.wahlkreis_id_2013
      JOIN partei p ON wpv.partei_id = p.id
      JOIN kandidat k_2013 ON wv.direktkandidat_2013 = k_2013.id
      JOIN kandidat k_2009 ON wv.direktkandidat_2009 = k_2009.id
      LEFT OUTER JOIN kandidat k_2017 ON wv.direktkandidat_2017 = k_2017.id
    ORDER BY wv.wahlkreis_id_2013, wpv.partei_id
  );