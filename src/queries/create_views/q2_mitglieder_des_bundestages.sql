CREATE OR REPLACE VIEW q2_mitglieder_des_bundestages (kandidat_vorname, kandidat_nachname, partei_name, b_name, mdb_listenplatz, jahr) AS
  (
    SELECT
      k.vor_name,
      k.nach_name,
      p.name AS partei_name,
      b.name AS bundesland_name,
      coalesce('' || mdb.listenplatz , 'Direkteinzug'),
      mdb.jahr   AS jahr
    FROM (SELECT *, 2013 as jahr
          FROM mitglieder_des_bundestags_2013
          UNION SELECT *, 2009 as jahr
                FROM mitglieder_des_bundestags_2009
          UNION SELECT *, 2017 as jahr
                FROM mitglieder_des_bundestags_2017) mdb
      JOIN kandidat k ON mdb.kandidat_id = k.id
      LEFT OUTER JOIN partei p ON mdb.partei_id = p.id
      JOIN bundesland b ON mdb.bundesland_id = b.id
    ORDER BY jahr DESC, partei_name, bundesland_name, listenplatz
  );