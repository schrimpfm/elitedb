DROP VIEW gesamtsitze_pro_partei_2017;

DROP MATERIALIZED VIEW gesamtsitze_pro_partei_und_bundesland_2017;

CREATE MATERIALIZED VIEW gesamtsitze_pro_partei_und_bundesland_2017(partei_id, bundesland_id, sitze) AS
  (
    WITH RECURSIVE ranking_pro_bundesland(bundesland_id, divisor, einwohner_ranking) AS
    (
      SELECT
        id,
        0.5 AS divisor,
        einwohner / 0.5
      FROM bundesland
      WHERE jahr = 2017
      UNION
      SELECT
        id,
        r.divisor + 1,
        einwohner / (r.divisor + 1)
      FROM bundesland b, ranking_pro_bundesland r
      WHERE b.id = r.bundesland_id AND b.jahr = 2017
            AND r.divisor < 600
--600 is more than enough therefore we don't have to change it in the second run
    ),
        sitze_bundesland_ranking AS
      (
          SELECT
            *,
            rank()
            OVER (
              ORDER BY einwohner_ranking DESC) AS ranking
          FROM ranking_pro_bundesland
      ),
        sitze_pro_bundesland(bundesland_id, sitze) AS
      (
          SELECT
            bundesland_id,
            count(*) AS sitze
          FROM sitze_bundesland_ranking
          WHERE ranking <= 598
          GROUP BY bundesland_id
      ),
        direktmandate(wahlkreis_id, kandidat_id) AS
      (
          SELECT
            w.wahlkreis_id,
            w.kandidat_id
          FROM wahlkreis_kandidaten_ergebnis_table w
            join wahlkreis wk on w.wahlkreis_id = wk.id
          WHERE wk.jahr = 2017 and stimmen = (SELECT max(stimmen)
                                              FROM wahlkreis_kandidaten_ergebnis_table w2
                                              WHERE w2.wahlkreis_id = w.wahlkreis_id)
      ),
        direktmandate_pro_partei_und_bundesland(partei_id, bundesland_id, direktmandate) AS
      (
          SELECT
            k.partei_id,
            w.bundesland_id,
            count(*)
          FROM direktmandate d
            JOIN kandidat k ON k.id = d.kandidat_id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE w.jahr = 2017 AND k.partei_id IS NOT NULL
          GROUP BY k.partei_id, w.bundesland_id
      ),
        stimmen_pro_partei(partei_id, zweitstimmen) AS
      (
          SELECT
            partei_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table
          WHERE wahlkreis_id IN
                (
                  SELECT
                    id
                  FROM wahlkreis
                  WHERE jahr = 2017
                )
          GROUP BY partei_id
      ),
        zugelassene_parteien(id) AS
      (
          SELECT
            p.id
          FROM partei p
            LEFT OUTER JOIN stimmen_pro_partei s ON p.id = s.partei_id
            LEFT OUTER JOIN (
                   SELECT
                     partei_id,
                     sum(direktmandate) AS direktmandate
                   FROM direktmandate_pro_partei_und_bundesland
                   GROUP BY partei_id
                 ) dpp ON p.id = dpp.partei_id
          WHERE dpp.direktmandate >= 3
                OR s.zweitstimmen >= 0.05 * (SELECT
                                               sum(zweitstimmen)
                                             FROM stimmen_pro_partei)
      ),
        stimmen_pro_zugelassener_partei AS
      (
          SELECT
            *
          FROM stimmen_pro_partei
          WHERE partei_id IN
                (
                  SELECT
                    id
                  FROM zugelassene_parteien
                )
      ),
        stimmen_pro_zugelassener_partei_und_bundesland(partei_id, bundesland_id, stimmen) AS
      (
          SELECT
            wpe.partei_id,
            w.bundesland_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table wpe
            JOIN wahlkreis w ON w.id = wpe.wahlkreis_id
          WHERE wpe.wahlkreis_id IN
                (
                  SELECT
                    id
                  FROM wahlkreis
                  WHERE jahr = 2017
                )
                AND wpe.partei_id IN
                    (
                      SELECT
                        id
                      FROM zugelassene_parteien
                    )
          GROUP BY wpe.partei_id, w.bundesland_id
      ),
        ranking_pro_partei_und_bundesland(partei_id, bundesland_id, divisor, stimmen_gesamt, stimmen_ranking) AS
      (
        SELECT
          wpe.partei_id,
          w.bundesland_id,
          0.5 AS divisor,
          sum(stimmen),
          sum(stimmen) / 0.5
        FROM wahlkreis_partei_ergebnis_table wpe
          JOIN wahlkreis w ON wpe.wahlkreis_id = w.id
        WHERE wpe.partei_id IN (SELECT
                                  id
                                FROM zugelassene_parteien)
        GROUP BY w.bundesland_id, wpe.partei_id
        UNION
        SELECT
          r.partei_id,
          r.bundesland_id,
          r.divisor + 1                      AS divisor,
          r.stimmen_gesamt,
          r.stimmen_gesamt / (r.divisor + 1) AS stimmen_ranking
        FROM ranking_pro_partei_und_bundesland r
          JOIN sitze_pro_bundesland s ON s.bundesland_id = r.bundesland_id
        WHERE divisor <= s.sitze
      ),
        sitze_partei_bundesland_ranking AS
      (
          SELECT
            r.*,
            rank()
            OVER (PARTITION BY r.bundesland_id
              ORDER BY r.stimmen_ranking DESC) AS ranking
          FROM ranking_pro_partei_und_bundesland r
      ),
        direktkandidaten_ohne_zugelassene_partei_pro_bundesland(kandidat_id, bundesland_id) AS
      (
          SELECT
            k.id,
            w.bundesland_id
          FROM direktmandate d
            JOIN kandidat k ON d.kandidat_id = k.id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE k.partei_id IS NULL OR k.partei_id NOT IN
                                       (
                                         SELECT
                                           id
                                         FROM zugelassene_parteien
                                       )
      ),
        sitze_pro_partei_und_bundesland(partei_id, bundesland_id, sitze) AS
      (
          SELECT
            spb.partei_id,
            spb.bundesland_id,
            count(*) AS sitze
          FROM sitze_partei_bundesland_ranking spb
            JOIN sitze_pro_bundesland s ON spb.bundesland_id = s.bundesland_id
          WHERE spb.ranking <= s.sitze - (SELECT count(*)
                                          FROM direktkandidaten_ohne_zugelassene_partei_pro_bundesland d
                                          WHERE d.bundesland_id = spb.bundesland_id)
          GROUP BY spb.partei_id, spb.bundesland_id
      ),
        mindestsitzzahl_pro_partei_und_bundesland(partei_id, bundesland_id, sitze) AS
      (
          SELECT
            coalesce(s.partei_id, d.partei_id),
            s.bundesland_id,
            greatest(s.sitze, d.direktmandate) AS sitze
          FROM sitze_pro_partei_und_bundesland s
            FULL OUTER JOIN direktmandate_pro_partei_und_bundesland d
              ON s.partei_id = d.partei_id AND s.bundesland_id = d.bundesland_id
          WHERE coalesce(s.partei_id, d.partei_id) IN
                (
                  SELECT
                    *
                  FROM zugelassene_parteien
                )
      ),
        ueberhangmandate_pro_partei_und_bundesland(partei_id, bundesland_id, ueberhang) AS
      (
          SELECT
            m.partei_id,
            m.bundesland_id,
            m.sitze - s.sitze AS ueberhang
          FROM mindestsitzzahl_pro_partei_und_bundesland m
            JOIN sitze_pro_partei_und_bundesland s ON m.partei_id = s.partei_id AND m.bundesland_id = s.bundesland_id
          WHERE m.sitze > s.sitze
      ),
        ueberhangmandate_pro_partei(partei_id, ueberhang) AS
      (
          SELECT
            partei_id,
            sum(ueberhang)
          FROM ueberhangmandate_pro_partei_und_bundesland
          GROUP BY partei_id
      ),
        mindestsitzzahl_pro_partei(partei_id, sitze) AS
      (
          SELECT
            partei_id,
            sum(sitze)
          FROM mindestsitzzahl_pro_partei_und_bundesland
          GROUP BY partei_id
      ),
        ranking_pro_partei(partei_id, divisor, stimmen_ranking) AS
      (
        SELECT
          partei_id,
          0.5 AS divisor,
          zweitstimmen / 0.5
        FROM stimmen_pro_zugelassener_partei
        UNION
        SELECT
          r.partei_id,
          r.divisor + 1,
          s.zweitstimmen / (r.divisor + 1)
        FROM stimmen_pro_zugelassener_partei s
          JOIN ranking_pro_partei r ON s.partei_id = r.partei_id
                                       AND r.divisor < 600
--600 is more than enough therefore we don't have to change it in the second run
      ),
        sitze_partei_ranking AS
      (
          SELECT
            *,
            rank()
            OVER (
              ORDER BY stimmen_ranking DESC) AS ranking
          FROM ranking_pro_partei
      ),
        stimmen_pro_partei_und_rang(partei_id, sitze, rang) AS
      (
        SELECT
          partei_id,
          0,
          0
        FROM sitze_partei_ranking
        UNION
        SELECT
          partei_id,
          sitze +
          (CASE
           WHEN (SELECT
                   partei_id
                 FROM sitze_partei_ranking
                 WHERE ranking = rang + 1) = partei_id
           THEN 1
           ELSE 0
           END),
          rang + 1
        FROM stimmen_pro_partei_und_rang
        WHERE rang + 1 <= (SELECT
                             max(ranking)
                           FROM sitze_partei_ranking)
      ),
        sitze_im_bundestag(sitze) AS
      (
          SELECT
            max(min_rang)
          FROM
            (SELECT
               spp.partei_id,
               min(spp.rang) min_rang
             FROM stimmen_pro_partei_und_rang spp JOIN
               mindestsitzzahl_pro_partei m ON spp.partei_id = m.partei_id
             WHERE spp.sitze >= m.sitze
             GROUP BY spp.partei_id
            ) mins
      ),
        sitze_pro_partei(partei_id, sitze) AS
      (
          SELECT
            partei_id,
            count(*) AS sitze
          FROM sitze_partei_ranking
          WHERE ranking <= (SELECT
                              *
                            FROM sitze_im_bundestag)
          GROUP BY partei_id
      ),
        gesamtsitze_pro_partei_ohne_ueberhang(partei_id, sitze) AS
      (
          SELECT
            spp.partei_id,
            spp.sitze - coalesce(u.ueberhang, 0)
          FROM sitze_pro_partei spp
            LEFT OUTER JOIN ueberhangmandate_pro_partei u ON spp.partei_id = u.partei_id
      ),
        gesamtranking_pro_partei_und_bundesland(partei_id, bundesland_id, gesamtstimmen, divisor, ranking) AS
      (
        SELECT
          partei_id,
          bundesland_id,
          stimmen       AS gesamtstimmen,
          0.5           AS divisor,
          stimmen / 0.5 AS ranking
        FROM stimmen_pro_zugelassener_partei_und_bundesland
        UNION
        SELECT
          gr.partei_id,
          gr.bundesland_id,
          gr.gesamtstimmen,
          gr.divisor + 1                      AS divisor,
          gr.gesamtstimmen / (1 + gr.divisor) AS ranking
        FROM gesamtranking_pro_partei_und_bundesland gr
          JOIN gesamtsitze_pro_partei_ohne_ueberhang gs ON gr.partei_id = gs.partei_id
        WHERE divisor <= gs.sitze
      ),
        gesamtsitze_pro_partei_und_bundesland_ohne_ueberhang(partei_id, bundesland_id, sitze) AS
      (
          SELECT
            psr.partei_id,
            psr.bundesland_id,
            count(*) AS sitze
          FROM
            (
              SELECT
                partei_id,
                bundesland_id,
                rank()
                OVER (PARTITION BY partei_id
                  ORDER BY ranking DESC) AS ranking
              FROM gesamtranking_pro_partei_und_bundesland
            ) psr
            JOIN gesamtsitze_pro_partei_ohne_ueberhang gs ON gs.partei_id = psr.partei_id
          WHERE psr.ranking <= gs.sitze
          GROUP BY psr.partei_id, psr.bundesland_id
      )
    SELECT
      gou.partei_id,
      gou.bundesland_id,
      gou.sitze + coalesce(uh.ueberhang, 0) AS sitze
    FROM gesamtsitze_pro_partei_und_bundesland_ohne_ueberhang gou
      LEFT OUTER JOIN ueberhangmandate_pro_partei_und_bundesland uh ON gou.partei_id = uh.partei_id AND gou.bundesland_id = uh.bundesland_id
  );

create view gesamtsitze_pro_partei_2017(partei_id, sitze) as
  (
    select partei_id, sum(sitze)
    from gesamtsitze_pro_partei_und_bundesland_2017
    group by partei_id
  );