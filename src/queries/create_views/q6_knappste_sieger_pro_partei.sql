CREATE OR REPLACE VIEW q6_knappste_sieger_pro_partei(
    sieger_partei,
    wahlkreis_name,
    sieger_vorname,
    sieger_nachname,
    stimmen_differenz,
    zweiter_partei,
    zweiter_vorname,
    zweiter_nachname,
    jahr) AS
  (
    WITH wahlkreis_kandidaten_ranking (wahlkreis_id, kandidat_id, stimmen, ranking, jahr) AS
    (
        SELECT
          wke.wahlkreis_id,
          wke.kandidat_id,
          wke.stimmen,
          rank()
          OVER (PARTITION BY wke.wahlkreis_id, jahr
            ORDER BY wke.stimmen DESC),
          jahr
        FROM wahlkreis_kandidaten_ergebnis_table wke
          JOIN wahlkreis w ON wke.wahlkreis_id = w.id
    ),
        wahlkreis_sieger (wahlkreis_id, kandidat_id, stimmen, jahr) AS
      (
          SELECT
            wahlkreis_id,
            kandidat_id,
            stimmen,
            jahr
          FROM wahlkreis_kandidaten_ranking
          WHERE ranking = 1
      ),
        wahlkreis_zweiter (wahlkreis_id, kandidat_id, stimmen, jahr) AS
      (
          SELECT
            wahlkreis_id,
            kandidat_id,
            stimmen,
            jahr
          FROM wahlkreis_kandidaten_ranking
          WHERE ranking = 2
      )
    SELECT
      sieger_partei,
      wahlkreis_name,
      sieger_vorname,
      sieger_nachname,
      stimmen_differenz,
      zweiter_partei,
      zweiter_vorname,
      zweiter_nachname,
      jahr
    FROM (
           SELECT
             p.name                                    AS sieger_partei,
             w.name                                    AS wahlkreis_name,
             k.vor_name                                AS sieger_vorname,
             k.nach_name                               AS sieger_nachname,
             ws.stimmen - wz.stimmen                   AS stimmen_differenz,
             kv.vor_name                               AS zweiter_vorname,
             kv.nach_name                              AS zweiter_nachname,
             pv.name                                   AS zweiter_partei,
             ws.jahr,
             rank()
             OVER (PARTITION BY p.id, p.jahr
               ORDER BY (ws.stimmen - wz.stimmen) ASC) AS knappheitsgrad
           FROM wahlkreis_sieger ws
             JOIN kandidat k ON ws.kandidat_id = k.id AND k.partei_id IS NOT NULL
             JOIN partei p ON k.partei_id = p.id
             JOIN wahlkreis_zweiter wz ON wz.wahlkreis_id = ws.wahlkreis_id
             JOIN kandidat kv ON wz.kandidat_id = kv.id
             JOIN partei pv ON kv.partei_id = pv.id
             JOIN wahlkreis w ON ws.wahlkreis_id = w.id) wsuz
    WHERE knappheitsgrad <= 10
    ORDER BY sieger_partei, stimmen_differenz
  );