CREATE OR REPLACE VIEW q5_ueberhangmandate_pro_partei(bundesland_name, partei_name, ueberhangmandate, jahr) AS (
  SELECT
    bundesland_name,
    partei_name,
    ueberhangmandate,
    2013 AS jahr
  FROM ueberhangmandate_pro_partei_2013
  UNION
  (
    SELECT
      bundesland_name,
      partei_name,
      ueberhangmandate,
      2009 AS jahr
    FROM ueberhangmandate_pro_partei_2009
  )
  UNION
  (
    SELECT
      bundesland_name,
      partei_name,
      ueberhangmandate,
      2017 AS jahr
    FROM ueberhangmandate_pro_partei_2017
  )
  ORDER BY jahr DESC
);

CREATE MATERIALIZED VIEW ueberhangmandate_pro_partei_2013(bundesland_name, partei_name, ueberhangmandate, jahr) AS
  (
    WITH RECURSIVE ranking_pro_bundesland(bundesland_id, divisor, einwohner_ranking) AS
    (
      SELECT
        id,
        0.5 AS divisor,
        einwohner / 0.5
      FROM bundesland
      WHERE jahr = 2013
      UNION
      SELECT
        id,
        r.divisor + 1,
        einwohner / (r.divisor + 1)
      FROM bundesland b, ranking_pro_bundesland r
      WHERE b.id = r.bundesland_id AND b.jahr = 2013
            AND r.divisor < 600
--600 is more than enough therefore we don't have to change it in the second run
    ),
        sitze_bundesland_ranking AS
      (
          SELECT
            *,
            rank()
            OVER (
              ORDER BY einwohner_ranking DESC) AS ranking
          FROM ranking_pro_bundesland
      ),
        sitze_pro_bundesland(bundesland_id, sitze) AS
      (
          SELECT
            bundesland_id,
            count(*) AS sitze
          FROM sitze_bundesland_ranking
          WHERE ranking <= 598
          GROUP BY bundesland_id
      ),
        max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen) AS
      (
          SELECT
            wahlkreis_id,
            max(stimmen)
          FROM wahlkreis_kandidaten_ergebnis_table
          GROUP BY wahlkreis_id
      ),
        direktmandate (wahlkreis_id, kandidat_id) AS
      (
          SELECT
            w.wahlkreis_id,
            w.kandidat_id
          FROM wahlkreis_kandidaten_ergebnis w
            join wahlkreis wk on w.wahlkreis_id = wk.id
          WHERE wk.jahr = 2013 and stimmen = (SELECT max(stimmen)
                                              FROM wahlkreis_kandidaten_ergebnis w2
                                              WHERE w2.wahlkreis_id = w.wahlkreis_id)
      ),
        direktmandate_pro_partei_und_bundesland(partei_id, bundesland_id, direktmandate) AS
      (
          SELECT
            k.partei_id,
            w.bundesland_id,
            count(*)
          FROM direktmandate d
            JOIN kandidat k ON k.id = d.kandidat_id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE w.jahr = 2013 AND k.partei_id IS NOT NULL
          GROUP BY k.partei_id, w.bundesland_id
      ),
        stimmen_pro_partei(partei_id, zweitstimmen) AS
      (
          SELECT
            partei_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table
          WHERE wahlkreis_id IN
                (
                  SELECT id
                  FROM wahlkreis
                  WHERE jahr = 2013
                )
          GROUP BY partei_id
      ),
        zugelassene_parteien(id) AS
      (
          SELECT p.id
          FROM partei p
            JOIN stimmen_pro_partei s ON p.id = s.partei_id
            LEFT OUTER JOIN (
                   SELECT
                     partei_id,
                     sum(direktmandate) AS direktmandate
                   FROM direktmandate_pro_partei_und_bundesland
                   GROUP BY partei_id
                 ) dpp ON p.id = dpp.partei_id
          WHERE dpp.direktmandate >= 3
                OR s.zweitstimmen >= 0.05 * (SELECT sum(zweitstimmen)
                                             FROM stimmen_pro_partei)
      ),
        stimmen_pro_zugelassener_partei AS
      (
          SELECT *
          FROM stimmen_pro_partei
          WHERE partei_id IN
                (
                  SELECT id
                  FROM zugelassene_parteien
                )
      ),
        stimmen_pro_zugelassener_partei_und_bundesland(partei_id, bundesland_id, stimmen) AS
      (
          SELECT
            wpe.partei_id,
            w.bundesland_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table wpe
            JOIN wahlkreis w ON w.id = wpe.wahlkreis_id
          WHERE wpe.wahlkreis_id IN
                (
                  SELECT id
                  FROM wahlkreis
                  WHERE jahr = 2013
                )
                AND wpe.partei_id IN
                    (
                      SELECT id
                      FROM zugelassene_parteien
                    )
          GROUP BY wpe.partei_id, w.bundesland_id
      ),
        ranking_pro_partei_und_bundesland(partei_id, bundesland_id, divisor, stimmen_gesamt, stimmen_ranking) AS
      (
        SELECT
          wpe.partei_id,
          w.bundesland_id,
          0.5 AS divisor,
          sum(stimmen),
          sum(stimmen) / 0.5
        FROM wahlkreis_partei_ergebnis_table wpe
          JOIN wahlkreis w ON wpe.wahlkreis_id = w.id
        WHERE wpe.partei_id IN (SELECT id
                                FROM zugelassene_parteien)
        GROUP BY w.bundesland_id, wpe.partei_id
        UNION
        SELECT
          r.partei_id,
          r.bundesland_id,
          r.divisor + 1                      AS divisor,
          r.stimmen_gesamt,
          r.stimmen_gesamt / (r.divisor + 1) AS stimmen_ranking
        FROM ranking_pro_partei_und_bundesland r
          JOIN sitze_pro_bundesland s ON s.bundesland_id = r.bundesland_id
        WHERE divisor <= s.sitze
      ),
        sitze_partei_bundesland_ranking AS
      (
          SELECT
            r.*,
            rank()
            OVER (PARTITION BY r.bundesland_id
              ORDER BY r.stimmen_ranking DESC) AS ranking
          FROM ranking_pro_partei_und_bundesland r
      ),
        direktkandidaten_ohne_zugelassene_partei_pro_bundesland(kandidat_id, bundesland_id) AS
      (
          SELECT
            k.id,
            w.bundesland_id
          FROM direktmandate d
            JOIN kandidat k ON d.kandidat_id = k.id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE k.partei_id IS NULL OR k.partei_id NOT IN
                                       (
                                         SELECT id
                                         FROM zugelassene_parteien
                                       )
      ),
        sitze_pro_partei_und_bundesland(partei_id, bundesland_id, sitze) AS
      (
          SELECT
            spb.partei_id,
            spb.bundesland_id,
            count(*) - (
              SELECT count(*)
              FROM direktkandidaten_ohne_zugelassene_partei_pro_bundesland d
              WHERE d.bundesland_id = spb.bundesland_id
            ) AS sitze
          FROM sitze_partei_bundesland_ranking spb
            JOIN sitze_pro_bundesland s ON spb.bundesland_id = s.bundesland_id
          WHERE spb.ranking <= s.sitze
          GROUP BY spb.partei_id, spb.bundesland_id
      )
    SELECT
      b.name AS bundesland_name,
      p.name AS partei_name,
      uppub.ueberhang,
      2013   AS jahr
    FROM
      (SELECT
         s.partei_id,
         s.bundesland_id,
         d.direktmandate - s.sitze AS ueberhang
       FROM sitze_pro_partei_und_bundesland s
         JOIN direktmandate_pro_partei_und_bundesland d
           ON s.partei_id = d.partei_id AND s.bundesland_id = d.bundesland_id
       WHERE d.direktmandate > s.sitze AND s.partei_id IN
                                           (
                                             SELECT *
                                             FROM zugelassene_parteien
                                           )) uppub
      JOIN partei p ON uppub.partei_id = p.id
      JOIN bundesland b ON uppub.bundesland_id = b.id
  );

CREATE MATERIALIZED VIEW ueberhangmandate_pro_partei_2009(bundesland_name, partei_name, ueberhangmandate) AS
  (
    WITH RECURSIVE ranking_pro_bundesland(bundesland_id, divisor, einwohner_ranking) AS
    (
      SELECT
        id,
        0.5 AS divisor,
        einwohner / 0.5
      FROM bundesland
      WHERE jahr = 2009
      UNION
      SELECT
        id,
        r.divisor + 1,
        einwohner / (r.divisor + 1)
      FROM bundesland b, ranking_pro_bundesland r
      WHERE b.id = r.bundesland_id AND b.jahr = 2009
            AND r.divisor < 600
--600 is more than enough therefore we don't have to change it in the second run
    ),
        sitze_bundesland_ranking AS
      (
          SELECT
            *,
            rank()
            OVER (
              ORDER BY einwohner_ranking DESC) AS ranking
          FROM ranking_pro_bundesland
      ),
        sitze_pro_bundesland(bundesland_id, sitze) AS
      (
          SELECT
            bundesland_id,
            count(*) AS sitze
          FROM sitze_bundesland_ranking
          WHERE ranking <= 598
          GROUP BY bundesland_id
      ),
        max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen) AS
      (
          SELECT
            wahlkreis_id,
            max(stimmen)
          FROM wahlkreis_kandidaten_ergebnis_table
          GROUP BY wahlkreis_id
      ),
        direktmandate (wahlkreis_id, kandidat_id) AS
      (
          SELECT
            w.wahlkreis_id,
            w.kandidat_id
          FROM wahlkreis_kandidaten_ergebnis w
            join wahlkreis wk on w.wahlkreis_id = wk.id
          WHERE wk.jahr = 2009 and stimmen = (SELECT max(stimmen)
                                              FROM wahlkreis_kandidaten_ergebnis w2
                                              WHERE w2.wahlkreis_id = w.wahlkreis_id)
      ),
        direktmandate_pro_partei_und_bundesland(partei_id, bundesland_id, direktmandate) AS
      (
          SELECT
            k.partei_id,
            w.bundesland_id,
            count(*)
          FROM direktmandate d
            JOIN kandidat k ON k.id = d.kandidat_id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE w.jahr = 2009 AND k.partei_id IS NOT NULL
          GROUP BY k.partei_id, w.bundesland_id
      ),
        stimmen_pro_partei(partei_id, zweitstimmen) AS
      (
          SELECT
            partei_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table
          WHERE wahlkreis_id IN
                (
                  SELECT id
                  FROM wahlkreis
                  WHERE jahr = 2009
                )
          GROUP BY partei_id
      ),
        zugelassene_parteien(id) AS
      (
          SELECT p.id
          FROM partei p
            JOIN stimmen_pro_partei s ON p.id = s.partei_id
            LEFT OUTER JOIN (
                   SELECT
                     partei_id,
                     sum(direktmandate) AS direktmandate
                   FROM direktmandate_pro_partei_und_bundesland
                   GROUP BY partei_id
                 ) dpp ON p.id = dpp.partei_id
          WHERE dpp.direktmandate >= 3
                OR s.zweitstimmen >= 0.05 * (SELECT sum(zweitstimmen)
                                             FROM stimmen_pro_partei)
      ),
        stimmen_pro_zugelassener_partei AS
      (
          SELECT *
          FROM stimmen_pro_partei
          WHERE partei_id IN
                (
                  SELECT id
                  FROM zugelassene_parteien
                )
      ),
        stimmen_pro_zugelassener_partei_und_bundesland(partei_id, bundesland_id, stimmen) AS
      (
          SELECT
            wpe.partei_id,
            w.bundesland_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table wpe
            JOIN wahlkreis w ON w.id = wpe.wahlkreis_id
          WHERE wpe.wahlkreis_id IN
                (
                  SELECT id
                  FROM wahlkreis
                  WHERE jahr = 2009
                )
                AND wpe.partei_id IN
                    (
                      SELECT id
                      FROM zugelassene_parteien
                    )
          GROUP BY wpe.partei_id, w.bundesland_id
      ),
        ranking_pro_partei_und_bundesland(partei_id, bundesland_id, divisor, stimmen_gesamt, stimmen_ranking) AS
      (
        SELECT
          wpe.partei_id,
          w.bundesland_id,
          0.5 AS divisor,
          sum(stimmen),
          sum(stimmen) / 0.5
        FROM wahlkreis_partei_ergebnis_table wpe
          JOIN wahlkreis w ON wpe.wahlkreis_id = w.id
        WHERE wpe.partei_id IN (SELECT id
                                FROM zugelassene_parteien)
        GROUP BY w.bundesland_id, wpe.partei_id
        UNION
        SELECT
          r.partei_id,
          r.bundesland_id,
          r.divisor + 1                      AS divisor,
          r.stimmen_gesamt,
          r.stimmen_gesamt / (r.divisor + 1) AS stimmen_ranking
        FROM ranking_pro_partei_und_bundesland r
          JOIN sitze_pro_bundesland s ON s.bundesland_id = r.bundesland_id
        WHERE divisor <= s.sitze
      ),
        sitze_partei_bundesland_ranking AS
      (
          SELECT
            r.*,
            rank()
            OVER (PARTITION BY r.bundesland_id
              ORDER BY r.stimmen_ranking DESC) AS ranking
          FROM ranking_pro_partei_und_bundesland r
      ),
        direktkandidaten_ohne_zugelassene_partei_pro_bundesland(kandidat_id, bundesland_id) AS
      (
          SELECT
            k.id,
            w.bundesland_id
          FROM direktmandate d
            JOIN kandidat k ON d.kandidat_id = k.id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE k.partei_id IS NULL OR k.partei_id NOT IN
                                       (
                                         SELECT id
                                         FROM zugelassene_parteien
                                       )
      ),
        sitze_pro_partei_und_bundesland(partei_id, bundesland_id, sitze) AS
      (
          SELECT
            spb.partei_id,
            spb.bundesland_id,
            count(*) - (
              SELECT count(*)
              FROM direktkandidaten_ohne_zugelassene_partei_pro_bundesland d
              WHERE d.bundesland_id = spb.bundesland_id
            ) AS sitze
          FROM sitze_partei_bundesland_ranking spb
            JOIN sitze_pro_bundesland s ON spb.bundesland_id = s.bundesland_id
          WHERE spb.ranking <= s.sitze
          GROUP BY spb.partei_id, spb.bundesland_id
      )
    SELECT
      b.name,
      p.name,
      uppub.ueberhang
    FROM
      (SELECT
         s.partei_id,
         s.bundesland_id,
         d.direktmandate - s.sitze AS ueberhang
       FROM sitze_pro_partei_und_bundesland s
         JOIN direktmandate_pro_partei_und_bundesland d
           ON s.partei_id = d.partei_id AND s.bundesland_id = d.bundesland_id
       WHERE d.direktmandate > s.sitze AND s.partei_id IN
                                           (
                                             SELECT *
                                             FROM zugelassene_parteien
                                           )) uppub
      JOIN partei p ON uppub.partei_id = p.id
      JOIN bundesland b ON uppub.bundesland_id = b.id
  );

CREATE MATERIALIZED VIEW ueberhangmandate_pro_partei_2017(bundesland_name, partei_name, ueberhangmandate) AS
  (
    WITH RECURSIVE ranking_pro_bundesland(bundesland_id, divisor, einwohner_ranking) AS
    (
      SELECT
        id,
        0.5 AS divisor,
        einwohner / 0.5
      FROM bundesland
      WHERE jahr = 2017
      UNION
      SELECT
        id,
        r.divisor + 1,
        einwohner / (r.divisor + 1)
      FROM bundesland b, ranking_pro_bundesland r
      WHERE b.id = r.bundesland_id AND b.jahr = 2017
            AND r.divisor < 600
--600 is more than enough therefore we don't have to change it in the second run
    ),
        sitze_bundesland_ranking AS
      (
          SELECT
            *,
            rank()
            OVER (
              ORDER BY einwohner_ranking DESC) AS ranking
          FROM ranking_pro_bundesland
      ),
        sitze_pro_bundesland(bundesland_id, sitze) AS
      (
          SELECT
            bundesland_id,
            count(*) AS sitze
          FROM sitze_bundesland_ranking
          WHERE ranking <= 598
          GROUP BY bundesland_id
      ),
        max_stimmen_pro_wahlkreis (wahlkreis_id, max_stimmen) AS
      (
          SELECT
            wahlkreis_id,
            max(stimmen)
          FROM wahlkreis_kandidaten_ergebnis_table
          GROUP BY wahlkreis_id
      ),
        direktmandate (wahlkreis_id, kandidat_id) AS
      (
          SELECT
            w.wahlkreis_id,
            w.kandidat_id
          FROM wahlkreis_kandidaten_ergebnis w
            join wahlkreis wk on w.wahlkreis_id = wk.id
          WHERE wk.jahr = 2017 and stimmen = (SELECT max(stimmen)
                                              FROM wahlkreis_kandidaten_ergebnis w2
                                              WHERE w2.wahlkreis_id = w.wahlkreis_id)
      ),
        direktmandate_pro_partei_und_bundesland(partei_id, bundesland_id, direktmandate) AS
      (
          SELECT
            k.partei_id,
            w.bundesland_id,
            count(*)
          FROM direktmandate d
            JOIN kandidat k ON k.id = d.kandidat_id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE w.jahr = 2017 AND k.partei_id IS NOT NULL
          GROUP BY k.partei_id, w.bundesland_id
      ),
        stimmen_pro_partei(partei_id, zweitstimmen) AS
      (
          SELECT
            partei_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table
          WHERE wahlkreis_id IN
                (
                  SELECT id
                  FROM wahlkreis
                  WHERE jahr = 2017
                )
          GROUP BY partei_id
      ),
        zugelassene_parteien(id) AS
      (
          SELECT p.id
          FROM partei p
            JOIN stimmen_pro_partei s ON p.id = s.partei_id
            LEFT OUTER JOIN (
                   SELECT
                     partei_id,
                     sum(direktmandate) AS direktmandate
                   FROM direktmandate_pro_partei_und_bundesland
                   GROUP BY partei_id
                 ) dpp ON p.id = dpp.partei_id
          WHERE dpp.direktmandate >= 3
                OR s.zweitstimmen >= 0.05 * (SELECT sum(zweitstimmen)
                                             FROM stimmen_pro_partei)
      ),
        stimmen_pro_zugelassener_partei AS
      (
          SELECT *
          FROM stimmen_pro_partei
          WHERE partei_id IN
                (
                  SELECT id
                  FROM zugelassene_parteien
                )
      ),
        stimmen_pro_zugelassener_partei_und_bundesland(partei_id, bundesland_id, stimmen) AS
      (
          SELECT
            wpe.partei_id,
            w.bundesland_id,
            sum(stimmen)
          FROM wahlkreis_partei_ergebnis_table wpe
            JOIN wahlkreis w ON w.id = wpe.wahlkreis_id
          WHERE wpe.wahlkreis_id IN
                (
                  SELECT id
                  FROM wahlkreis
                  WHERE jahr = 2017
                )
                AND wpe.partei_id IN
                    (
                      SELECT id
                      FROM zugelassene_parteien
                    )
          GROUP BY wpe.partei_id, w.bundesland_id
      ),
        ranking_pro_partei_und_bundesland(partei_id, bundesland_id, divisor, stimmen_gesamt, stimmen_ranking) AS
      (
        SELECT
          wpe.partei_id,
          w.bundesland_id,
          0.5 AS divisor,
          sum(stimmen),
          sum(stimmen) / 0.5
        FROM wahlkreis_partei_ergebnis_table wpe
          JOIN wahlkreis w ON wpe.wahlkreis_id = w.id
        WHERE wpe.partei_id IN (SELECT id
                                FROM zugelassene_parteien)
        GROUP BY w.bundesland_id, wpe.partei_id
        UNION
        SELECT
          r.partei_id,
          r.bundesland_id,
          r.divisor + 1                      AS divisor,
          r.stimmen_gesamt,
          r.stimmen_gesamt / (r.divisor + 1) AS stimmen_ranking
        FROM ranking_pro_partei_und_bundesland r
          JOIN sitze_pro_bundesland s ON s.bundesland_id = r.bundesland_id
        WHERE divisor <= s.sitze
      ),
        sitze_partei_bundesland_ranking AS
      (
          SELECT
            r.*,
            rank()
            OVER (PARTITION BY r.bundesland_id
              ORDER BY r.stimmen_ranking DESC) AS ranking
          FROM ranking_pro_partei_und_bundesland r
      ),
        direktkandidaten_ohne_zugelassene_partei_pro_bundesland(kandidat_id, bundesland_id) AS
      (
          SELECT
            k.id,
            w.bundesland_id
          FROM direktmandate d
            JOIN kandidat k ON d.kandidat_id = k.id
            JOIN wahlkreis w ON w.id = d.wahlkreis_id
          WHERE k.partei_id IS NULL OR k.partei_id NOT IN
                                       (
                                         SELECT id
                                         FROM zugelassene_parteien
                                       )
      ),
        sitze_pro_partei_und_bundesland(partei_id, bundesland_id, sitze) AS
      (
          SELECT
            spb.partei_id,
            spb.bundesland_id,
            count(*) - (
              SELECT count(*)
              FROM direktkandidaten_ohne_zugelassene_partei_pro_bundesland d
              WHERE d.bundesland_id = spb.bundesland_id
            ) AS sitze
          FROM sitze_partei_bundesland_ranking spb
            JOIN sitze_pro_bundesland s ON spb.bundesland_id = s.bundesland_id
          WHERE spb.ranking <= s.sitze
          GROUP BY spb.partei_id, spb.bundesland_id
      )
    SELECT
      b.name,
      p.name,
      uppub.ueberhang
    FROM
      (SELECT
         s.partei_id,
         s.bundesland_id,
         d.direktmandate - s.sitze AS ueberhang
       FROM sitze_pro_partei_und_bundesland s
         JOIN direktmandate_pro_partei_und_bundesland d
           ON s.partei_id = d.partei_id AND s.bundesland_id = d.bundesland_id
       WHERE d.direktmandate > s.sitze AND s.partei_id IN
                                           (
                                             SELECT *
                                             FROM zugelassene_parteien
                                           )) uppub
      JOIN partei p ON uppub.partei_id = p.id
      JOIN bundesland b ON uppub.bundesland_id = b.id
  );