with recursive ranking_pro_bundesland(bundesland_id, divisor, einwohner_ranking) as
(
	select id, 0.5 as divisor, einwohner/0.5
	from bundesland
	where jahr = 2013
union
	select id, r.divisor + 1, einwohner/(r.divisor + 1)
	from bundesland b, ranking_pro_bundesland r
	where b.id = r.bundesland_id and b.jahr = 2013
	and r.divisor < 598
),
sitze_ranking as
(
	select *, rank() over (order by einwohner_ranking desc) as ranking
	from ranking_pro_bundesland
)
select b.name, count(*) as sitze
from sitze_ranking s, bundesland b
where s.bundesland_id = b.id 
and ranking <= 598
group by bundesland_id, b.name

with sitze_ranking as
(
select *, rank() over (order by einwohner_ranking asc) as ranking
from ranking_pro_bundesland
),
sitze_pro_bundesland as
(
select bundesland_id, count(*) as sitze
from sitze_ranking
where ranking <= 598
group by bundesland_id
)
with recursive view ranking_pro_partei_und_bundesland(partei_id, bundesland_id, divisor, einwohner_ranking) as
(
select id, 0.5 as divisor, einwohner/0.5
from bundesland
union
select id, r.divisor + 1, einwohner/(r.divisor + 1)
from bundesland b, ranking_pro_bundesland r
where b.id = s.bundesland_id
and r.divisor < 598
)