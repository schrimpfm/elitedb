CREATE OR REPLACE VIEW q1_sitzverteilung_im_bundestag(partei_name, sitze, jahr) AS
  (
    SELECT
      p.name,
      gpp.sitze,
      2013 AS jahr
    FROM gesamtsitze_pro_partei_2013 gpp
      JOIN partei p ON gpp.partei_id = p.id
    UNION
    (SELECT
       p.name,
       gpp.sitze,
       2009 AS jahr
     FROM gesamtsitze_pro_partei_2009 gpp
       JOIN partei p ON gpp.partei_id = p.id)
    UNION
    (SELECT
       p.name,
       gpp.sitze,
       2017 AS jahr
     FROM gesamtsitze_pro_partei_2017 gpp
       JOIN partei p ON gpp.partei_id = p.id)
    ORDER BY jahr DESC, sitze DESC
  );