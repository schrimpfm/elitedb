﻿-- Default encoding in postgres is utf8 which is not changed here

DROP TRIGGER Refresh_stimmen_aggregate ON Wahlzettel;
DROP TRIGGER Refresh_aggregate ON Anonymizer;
DROP FUNCTION refresh_wahlzettel_aggregate();
DROP FUNCTION increase_wahlzettel_trigger();
DROP MATERIALIZED VIEW Wahlkreis_kandidaten_ergebnis;
DROP MATERIALIZED VIEW Wahlkreis_partei_ergebnis;
DROP TABLE Anonymizer;
DROP TABLE Erststimmen_aggregiert;
DROP TABLE Zweitstimmen_aggregiert;
DROP TABLE Wahlzettel;
DROP TABLE Wahlkreisliste;
DROP TABLE Landesliste;
DROP TABLE Kandidat;
DROP TABLE Wahlkreis;
DROP TABLE Sitze_pro_partei_und_bl;
DROP TABLE Partei;
DROP TABLE Bundesland;
DROP TABLE Konstanten;
DROP TABLE Wahljahr;

CREATE TABLE Wahljahr (
  jahr INTEGER NOT NULL PRIMARY KEY
);

CREATE TABLE Konstanten (
  gesamt_sitze               INTEGER,
  prozent_huerde             DECIMAL(4, 3),
  mandat_huerde              INTEGER,
  keycode_valid_time_seconds INTEGER
);

CREATE TABLE Bundesland (
  id               SERIAL,
  name             VARCHAR(30) NOT NULL,
  divisor          DECIMAL(20, 19),
  stimmen_pro_sitz DECIMAL(20, 19),
  sitze_im_bt      INTEGER,
  sitze_mit_um     INTEGER,
  sitze_mit_am     INTEGER,
  jahr             INTEGER,
  einwohner        INTEGER     NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (name, jahr),
  FOREIGN KEY (jahr) REFERENCES Wahljahr (jahr)
);

CREATE TABLE Partei (
  id   SERIAL,
  name VARCHAR(50) NOT NULL,
  jahr INTEGER,
  PRIMARY KEY (id),
  UNIQUE (name, jahr),
  FOREIGN KEY (jahr) REFERENCES Wahljahr (jahr)
);

CREATE TABLE Sitze_pro_partei_und_bl (
  partei_id     INTEGER NOT NULL,
  bundesland_id INTEGER NOT NULL,
  sitze_im_bt   INTEGER,
  sitze_mit_um  INTEGER,
  sitze_mit_am  INTEGER,
  PRIMARY KEY (partei_id, bundesland_id),
  FOREIGN KEY (partei_id) REFERENCES Partei (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (bundesland_id) REFERENCES Bundesland (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Wahlkreis (
  id              SERIAL,
  wahlkreisnummer INTEGER      NOT NULL,
  name            VARCHAR(100) NOT NULL,
  bundesland_id   INTEGER      NOT NULL,
  wahlberechtigte INTEGER      NOT NULL,
  jahr            INTEGER      NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (wahlkreisnummer, jahr),
  FOREIGN KEY (bundesland_id) REFERENCES Bundesland (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (jahr) REFERENCES Wahljahr (jahr)
);

CREATE TABLE Kandidat (
  id          SERIAL,
  vor_name    VARCHAR(50) NOT NULL,
  nach_name   VARCHAR(50) NOT NULL,
  geburtsjahr INTEGER,
  partei_id   INTEGER,
  PRIMARY KEY (id),
  FOREIGN KEY (partei_id) REFERENCES Partei (id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE Landesliste (
  bundesland_id INTEGER NOT NULL,
  partei_id     INTEGER NOT NULL,
  prio          INTEGER NOT NULL,
  kandidat_id   INTEGER NOT NULL,
  PRIMARY KEY (bundesland_id, partei_id, prio),
  FOREIGN KEY (bundesland_id) REFERENCES Bundesland (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (partei_id) REFERENCES Partei (id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (kandidat_id) REFERENCES Kandidat (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Wahlkreisliste (
  wahlkreis_id INTEGER NOT NULL,
  kandidat_id  INTEGER NOT NULL,
  PRIMARY KEY (wahlkreis_id, kandidat_id),
  FOREIGN KEY (wahlkreis_id) REFERENCES Wahlkreis (id),
  FOREIGN KEY (kandidat_id) REFERENCES Kandidat (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Wahlzettel (
  id           SERIAL,
  wahlkreis_id INTEGER NOT NULL,
  erst_stimme  INTEGER,
  zweit_stimme INTEGER,
  PRIMARY KEY (id),
  FOREIGN KEY (wahlkreis_id) REFERENCES wahlkreis (id),
  FOREIGN KEY (erst_stimme) REFERENCES Kandidat (id),
  FOREIGN KEY (zweit_stimme) REFERENCES Partei (id)
);

CREATE TABLE Zweitstimmen_aggregiert (
  wahlkreis_id INTEGER NOT NULL,
  partei_id    INTEGER NOT NULL,
  stimmen      INTEGER,
  PRIMARY KEY (wahlkreis_id, partei_id),
  FOREIGN KEY (wahlkreis_id) REFERENCES wahlkreis (id),
  FOREIGN KEY (partei_id) REFERENCES Partei (id)
);

CREATE TABLE Erststimmen_aggregiert (
  wahlkreis_id INTEGER NOT NULL,
  kandidat_id  INTEGER NOT NULL,
  stimmen      INTEGER,
  PRIMARY KEY (wahlkreis_id, kandidat_id),
  FOREIGN KEY (wahlkreis_id) REFERENCES wahlkreis (id),
  FOREIGN KEY (kandidat_id) REFERENCES Kandidat (id)
);

CREATE TABLE Anonymizer (
  wahlzettel_trigger INTEGER NOT NULL
);

CREATE MATERIALIZED VIEW Wahlkreis_partei_ergebnis(wahlkreis_id, partei_id, stimmen) AS
  (
    SELECT
      wz.wahlkreis_id,
      wz.zweit_stimme,
      count(*)
    FROM Wahlzettel wz
    WHERE zweit_stimme IS NOT NULL
    GROUP BY wz.wahlkreis_id, wz.zweit_stimme
  );

CREATE MATERIALIZED VIEW Wahlkreis_kandidaten_ergebnis(wahlkreis_id, kandidat_id, stimmen) AS
  (
    SELECT
      wz.wahlkreis_id,
      wz.erst_stimme,
      count(*)
    FROM Wahlzettel wz
    WHERE erst_stimme IS NOT NULL
    GROUP BY wz.wahlkreis_id, wz.erst_stimme
  );

CREATE FUNCTION increase_wahlzettel_trigger()
  RETURNS TRIGGER
AS $increase_wahlzettel_trigger$
DECLARE counter INTEGER;
BEGIN
  SELECT count(*)
  FROM Anonymizer
  INTO counter;

  IF (counter = 0)
  THEN
    INSERT INTO Anonymizer VALUES (1);
  ELSE
    UPDATE Anonymizer
    SET wahlzettel_trigger = wahlzettel_trigger + 1;
  END IF;
  RETURN NEW;
END;
$increase_wahlzettel_trigger$ LANGUAGE plpgsql;

CREATE TRIGGER Refresh_stimmen_aggregate
AFTER INSERT ON Wahlzettel
FOR EACH ROW
EXECUTE PROCEDURE increase_wahlzettel_trigger();

CREATE FUNCTION refresh_wahlzettel_aggregate()
  RETURNS TRIGGER
AS $refresh_wahlzettel_aggregate$
BEGIN
  IF (new.wahlzettel_trigger >= 100)
  THEN
    new.wahlzettel_trigger = 0;
    REFRESH MATERIALIZED VIEW Wahlkreis_partei_ergebnis;
    REFRESH MATERIALIZED VIEW Wahlkreis_kandidaten_ergebnis;
  END IF;
  RETURN NEW;
END;
$refresh_wahlzettel_aggregate$ LANGUAGE plpgsql;

CREATE TRIGGER Refresh_aggregate
AFTER UPDATE ON Anonymizer
FOR EACH ROW
EXECUTE PROCEDURE refresh_wahlzettel_aggregate();

GRANT ALL PRIVILEGES ON Anonymizer TO developers;
GRANT ALL PRIVILEGES ON Erststimmen_aggregiert TO developers;
GRANT ALL PRIVILEGES ON Zweitstimmen_aggregiert TO developers;
GRANT ALL PRIVILEGES ON Wahlzettel TO developers;
GRANT ALL PRIVILEGES ON Wahlkreisliste TO developers;
GRANT ALL PRIVILEGES ON Landesliste TO developers;
GRANT ALL PRIVILEGES ON Kandidat TO developers;
GRANT ALL PRIVILEGES ON Wahlkreis TO developers;
GRANT ALL PRIVILEGES ON Sitze_pro_partei_und_bl TO developers;
GRANT ALL PRIVILEGES ON Partei TO developers;
GRANT ALL PRIVILEGES ON Bundesland TO developers;
GRANT ALL PRIVILEGES ON Konstanten TO developers;
GRANT ALL PRIVILEGES ON Wahljahr TO developers;