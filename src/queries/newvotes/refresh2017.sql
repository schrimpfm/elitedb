CREATE OR REPLACE FUNCTION refresh2017()
  RETURNS BOOL
AS $refresh2017$
DECLARE
  wahlkreis_ids_2017 INTEGER[];
BEGIN
  SELECT into wahlkreis_ids_2017 ARRAY (SELECT id
                                    FROM wahlkreis
                                    WHERE jahr = 2017);
  DELETE FROM wahlkreis_kandidaten_ergebnis_table
    WHERE wahlkreis_id = ANY (wahlkreis_ids_2017);

  DELETE FROM wahlkreis_partei_ergebnis_table
    WHERE wahlkreis_id = ANY (wahlkreis_ids_2017);

  INSERT INTO wahlkreis_kandidaten_ergebnis_table (wahlkreis_id, kandidat_id, stimmen)
    SELECT wahlkreis_id, erst_stimme, count(*)
    FROM wahlzettel
    WHERE wahlkreis_id = ANY (wahlkreis_ids_2017)
    GROUP BY wahlkreis_id, erst_stimme;

  INSERT INTO wahlkreis_partei_ergebnis_table (wahlkreis_id, partei_id, stimmen)
    SELECT wahlkreis_id, zweit_stimme, count(*)
    FROM wahlzettel
    WHERE wahlkreis_id = ANY (wahlkreis_ids_2017)
    GROUP BY wahlkreis_id, zweit_stimme;

  REFRESH MATERIALIZED VIEW gesamtsitze_pro_partei_und_bundesland_2017;
  REFRESH MATERIALIZED VIEW mitglieder_des_bundestags_2017;
  REFRESH MATERIALIZED VIEW ueberhangmandate_pro_partei_2017;
  RETURN TRUE;
END;
$refresh2017$ LANGUAGE plpgsql;

