CREATE TABLE keycodes (
  keycode       CHAR(16) UNIQUE,
  status        INTEGER,
  wahlkreis_id  INTEGER,
  creation_time TIMESTAMP,
  FOREIGN KEY (wahlkreis_id) REFERENCES wahlkreis (id)
);

CREATE INDEX status_index ON keycodes (status);
CREATE INDEX keycode_index ON keycodes USING HASH (keycode);
CREATE INDEX keycode_wahlkreis_index ON keycodes (keycode, wahlkreis_id);

CREATE TABLE abgegebene_stimmen (
  id           SERIAL,
  wahlkreis_id INTEGER,
  erst_stimme  INTEGER,
  zweit_stimme INTEGER,
  keycode      CHAR(16)
);

CREATE TABLE pseudo_wahlzettel (
  wahlkreis_id INTEGER,
  erst_stimme  INTEGER,
  zweit_stimme INTEGER
);

CREATE OR REPLACE FUNCTION copy_to_wahlzettel()
  RETURNS TRIGGER
AS $copy_to_wahlzettel$
DECLARE
  valid BOOLEAN;
BEGIN
  SELECT INTO valid count(*) > 0
  FROM keycodes k
    JOIN wahlkreisliste wl ON k.wahlkreis_id = wl.wahlkreis_id
    JOIN wahlkreis w ON w.id = k.wahlkreis_id
    JOIN landesliste l ON w.bundesland_id = l.bundesland_id,
    konstanten kons
  WHERE k.keycode = new.keycode AND k.wahlkreis_id = new.wahlkreis_id AND k.status = 1
        AND wl.kandidat_id = new.erst_stimme AND l.partei_id = new.zweit_stimme AND extract(EPOCH FROM (now() - k.creation_time)) < kons.keycode_valid_time_seconds
        AND w.jahr = 2017;

  IF (valid)
  THEN
    INSERT INTO wahlzettel (wahlkreis_id, erst_stimme, zweit_stimme)
      SELECT
        wahlkreis_id,
        erst_stimme,
        zweit_stimme
      FROM abgegebene_stimmen
      WHERE keycode = new.keycode;
    UPDATE keycodes
    SET status = 2
    WHERE keycode = new.keycode AND wahlkreis_id = new.wahlkreis_id;
  END IF;
  RETURN NEW;
END;
$copy_to_wahlzettel$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TRIGGER copy_to_wahlzettel_trigger
AFTER INSERT ON abgegebene_stimmen
FOR EACH ROW
EXECUTE PROCEDURE copy_to_wahlzettel();

CREATE OR REPLACE FUNCTION getKeycode(in_wahlkreis_id INTEGER)
  RETURNS CHAR(16)
AS $getKeycode$
DECLARE
  wahlkreis_valid BOOLEAN;
  code            CHAR(16);
BEGIN
  SELECT INTO wahlkreis_valid count(*) > 0
  FROM wahlkreis
  WHERE id = in_wahlkreis_id AND jahr = 2017;
  IF (wahlkreis_valid)
  THEN
    SELECT INTO code keycode
    FROM keycodes
    WHERE status = 0;
    IF (NOT code IS NULL)
    THEN
      UPDATE keycodes
      SET status = 1, wahlkreis_id = in_wahlkreis_id, creation_time = now()
      WHERE keycode = code;
    END IF;
    RETURN code;
  ELSE
    RETURN -1;
  END IF;
END;
$getKeycode$ LANGUAGE plpgsql;
