package parsing;

import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import util.Pair;
import file.FileReader;
import file.FileWriter;

public class Parser {

	private int numberOfWahlkreise;
	private TreeSet<String> parteien;

	private HashMap<String, Integer> parteienMitId;
	private HashMap<String, Integer> bundeslaenderMitId;

	private HashMap<Pair<Integer, Integer>, Integer> wahlkreisIdAndParteiIdToKandidatId;
	private HashMap<Pair<Integer, String>, Integer> wahlkreisIdAndNachnameToKandidatId;
	private HashMap<Pair<String, String>, Integer> vornameAndNachnameToKandidatId;

	private HashMap<Integer, Integer> wahlkreisIdToBundeslandId;
	private HashMap<Pair<Integer, Integer>, Integer> wahlkreisNrAndYearToWahlkreisId;

	private final StringBuilder completeScript;

	public Parser() {
		init();

		completeScript = new StringBuilder();
	}

	private void init() {
		parteien = new TreeSet<String>();
		numberOfWahlkreise = 0;
		parteien.add("'Ung�ltige'");
		parteienMitId = new HashMap<String, Integer>();
		bundeslaenderMitId = new HashMap<String, Integer>();
		wahlkreisIdAndParteiIdToKandidatId = new HashMap<Pair<Integer, Integer>, Integer>();
		wahlkreisIdAndNachnameToKandidatId = new HashMap<Pair<Integer, String>, Integer>();
		vornameAndNachnameToKandidatId = new HashMap<Pair<String, String>, Integer>();
		wahlkreisIdToBundeslandId = new HashMap<Integer, Integer>();
		wahlkreisNrAndYearToWahlkreisId = new HashMap<Pair<Integer, Integer>, Integer>();
	}

	public void parse() {
		final int year2009 = 2009;
		final int year2013 = 2013;
		init();
		completeScript.append(""
				+ "insert into Wahljahr values(2009),(2013);\n\n");

		parseBundeslaenderWahlkreise(year2009);
		// parseLandeslisten(year2009);
		parseKandidaten(year2009);
		parseZweitstimmen(year2009);
		parseErststimmen(year2009);

		init();

		parseBundeslaenderWahlkreise(year2013);
		// parseLandeslisten(year2013);
		parseKandidaten(year2013);
		parseZweitstimmen(year2013);
		parseErststimmen(year2013);

		final FileWriter completeScriptWriter = new
				FileWriter("data/complete.sql");
		completeScriptWriter.write(completeScript.toString());
	}

	private void parseBundeslaenderWahlkreise(final int year) {
		final String wahlkreiseMitWahlberechtigtenPath = "data/db_wahl_" + year + "/wahlkreise_mit_wahlberechtigten.csv";
		final String bundeslaenderMitEinwohnernPath = "data/db_wahl_" + year + "/bundeslaender_mit_einwohnern.csv";

		final FileReader reader = new FileReader(wahlkreiseMitWahlberechtigtenPath);
		final List<String> lines = reader.getLines();

		final FileReader einwohnerReader = new FileReader(bundeslaenderMitEinwohnernPath);
		final List<String> einwohnerLines = einwohnerReader.getLines();

		final String bundeslaenderPath = "data/db_wahl_" + year + "/bundeslaender.sql";
		final String wahlkreisePath = "data/db_wahl_" + year + "/wahlkreise.sql";

		final FileWriter bundeslaenderWriter = new FileWriter(bundeslaenderPath);
		final FileWriter wahlkreiseWriter = new FileWriter(wahlkreisePath);

		final StringBuilder bundeslaenderString = new StringBuilder();
		final StringBuilder wahlkreiseString = new StringBuilder();

		bundeslaenderString.append("insert into Bundesland(name, jahr, einwohner) \nvalues \n");
		wahlkreiseString.append("insert into Wahlkreis(wahlkreisnummer, name, bundesland_id, wahlberechtigte, jahr) \nvalues \n");

		final TreeSet<String> bundeslaender = new TreeSet<String>();

		final HashMap<String, Integer> einwohnerProBundesland = new HashMap<String, Integer>();
		for (final String line : einwohnerLines) {
			final String[] bundeslandEinwohner = line.split(";", -1);
			einwohnerProBundesland.put(getStringWithQuotation(bundeslandEinwohner[0]), Integer.valueOf(bundeslandEinwohner[1]));
		}

		for (final String line : lines) {
			final String[] parts = line.replace("\"", "'").split(";", -1);
			bundeslaender.add(parts[1]);
		}

		int index = getBundeslandOffset(year);
		for (final String bundesland : bundeslaender) {
			bundeslaenderMitId.put(getStringWithQuotation(bundesland), Integer.valueOf(index));
			if (index > getBundeslandOffset(year)) {
				bundeslaenderString.append(",\n");
			}
			final Integer einwohner = einwohnerProBundesland.get(getStringWithQuotation(bundesland));
			bundeslaenderString.append("(" + getStringWithQuotation(bundesland) + ", " + year + ", " + einwohner + ")");
			index++;
		}

		final Integer yearInt = Integer.valueOf(year);
		index = getWahlkreisOffset(year);
		for (final String line : lines) {
			final String[] parts = line.replace("\"", "'").split(";", -1);
			bundeslaender.add(parts[1]);
			final Integer bundeslandId = bundeslaenderMitId.get(getStringWithQuotation(parts[1]));
			if (index > getWahlkreisOffset(year)) {
				wahlkreiseString.append(",\n");
			}
			final Integer wahlkreisId = new Integer(parts[2]);
			if (wahlkreisId.intValue() > numberOfWahlkreise) {
				numberOfWahlkreise = wahlkreisId.intValue();
			}
			wahlkreiseString.append("(" + wahlkreisId + ", " + getStringWithQuotation(parts[3]) + ", " + bundeslandId + ", " + parts[4] + ", " + year + ")");
			wahlkreisNrAndYearToWahlkreisId.put(new Pair<Integer, Integer>(wahlkreisId, yearInt), Integer.valueOf(index));
			wahlkreisIdToBundeslandId.put(wahlkreisId, bundeslandId);
			index++;
		}

		completeScript.append(bundeslaenderString.toString() + ";\n\n");
		completeScript.append(wahlkreiseString.toString() + ";\n\n");

		bundeslaenderWriter.write(bundeslaenderString.toString());
		wahlkreiseWriter.write(wahlkreiseString.toString());
	}

	// private void parseLandeslisten(final int year) {
	// final String landeslistenPath = "data/db_wahl_" + year +
	// "/landeslisten.csv";
	//
	// final FileReader reader = new FileReader(landeslistenPath);
	// final List<String> lines = reader.getLines();
	//
	// for (final String line : lines) {
	// final String[] parts = line.replace("\"", "'").split(";", -1);
	// parteien.add(getUniqueParteinameWithQuotation(parts[3]));
	// bundeslandIdAndParteiToLandeslisteId.put(new Pair<Integer,
	// String>(bundeslaenderMitId.get(getStringWithQuotation(parts[1])),
	// parts[2]), Integer.valueOf(parts[1]));
	// }
	// }

	private void parseKandidaten(final int year) {
		final String wahlbewerberPath = "data/db_wahl_" + year + "/wahlbewerber.csv";

		final FileReader reader = new FileReader(wahlbewerberPath);
		final List<String> lines = reader.getLines();

		final String kandidatenPath = "data/db_wahl_" + year + "/kandidaten.sql";
		final String parteienPath = "data/db_wahl_" + year + "/parteien.sql";
		final String landeslistePath = "data/db_wahl_" + year + "/landeslisten.sql";
		final String wahlkreislistePath = "data/db_wahl_" + year + "/wahlkreisliste.sql";

		final FileWriter kandidatenWriter = new FileWriter(kandidatenPath);
		final FileWriter parteienWriter = new FileWriter(parteienPath);
		final FileWriter landeslisteWriter = new FileWriter(landeslistePath);
		final FileWriter wahlkreislisteWriter = new FileWriter(wahlkreislistePath);

		final StringBuilder kandidatenString = new StringBuilder();
		final StringBuilder parteienString = new StringBuilder();
		final StringBuilder landeslisteString = new StringBuilder();
		final StringBuilder wahlkreislisteString = new StringBuilder();

		kandidatenString.append("insert into Kandidat(vor_name, nach_name, geburtsjahr, partei_id) \nvalues \n");
		parteienString.append("insert into Partei(name, jahr) \nvalues \n");
		landeslisteString.append("insert into Landesliste(bundesland_id, partei_id, prio, kandidat_id) \nvalues \n");
		wahlkreislisteString.append("insert into Wahlkreisliste(wahlkreis_id, kandidat_id) \nvalues");

		for (final String line : lines) {
			final String[] parts = line.replace("\"", "'").split(";", -1);
			final String parteiName = getUniqueParteinameWithQuotation(parts[3]);
			if (parteiName != null)
				parteien.add(parteiName);
		}

		int index = getParteiOffset(year);
		for (final String partei : parteien) {
			parteienMitId.put(partei, Integer.valueOf(index));
			if (index > getParteiOffset(year)) {
				parteienString.append(",\n");
			}
			parteienString.append("(" + partei + ", " + year + ")");
			index++;
		}

		final Integer yearInt = new Integer(year);
		index = getKandidatOffset(year);
		int landeslistenCount = 1;
		int wahlkreislistenCount = 1;
		for (final String line : lines) {
			final String[] parts = line.replace("\"", "'").split(";", -1);
			if (index > getKandidatOffset(year)) {
				kandidatenString.append(",\n");
			}
			final String vorname = parts[0];
			final String nachname = parts[1];
			final Integer jahrgang = new Integer(parts[2]);
			final Integer parteiId = parteienMitId.get(getUniqueParteinameWithQuotation(parts[3]));
			kandidatenString.append("(" +
					getStringWithQuotation(vorname) + ", " +
					getStringWithQuotation(nachname) + ", " +
					jahrgang + ", " +
					parteiId + ")");

			final Integer wahlkreis = parts[4].isEmpty() ? null : new Integer(parts[4]);
			final Integer bundesland = bundeslaenderMitId.get(getStringWithQuotation(parts[5]));
			final Integer listenplatz = parts[6].isEmpty() ? null : new Integer(parts[6]);

			if (listenplatz != null) {
				if (landeslistenCount > 1) {
					landeslisteString.append(",\n");
				}
				landeslisteString.append("(" + bundesland + ", " + parteiId + ", " + listenplatz + ", " + index + ")");
				landeslistenCount++;
			}
			if (wahlkreis != null) {
				if (wahlkreislistenCount > 1) {
					wahlkreislisteString.append(",\n");
				}
				final Integer indexInt = new Integer(index);
				wahlkreislisteString.append("(" + wahlkreisNrAndYearToWahlkreisId.get(new Pair<Integer, Integer>(wahlkreis, yearInt)) + ", " + index + ")");
				wahlkreisIdAndParteiIdToKandidatId.put(new Pair<Integer, Integer>(wahlkreis, parteiId), indexInt);
				wahlkreisIdAndNachnameToKandidatId.put(new Pair<Integer, String>(wahlkreis, getStringWithQuotation(nachname)), indexInt);
				vornameAndNachnameToKandidatId.put(new Pair<String, String>(getStringWithQuotation(vorname), getStringWithQuotation(nachname)), indexInt);
				wahlkreislistenCount++;
			}
			index++;
		}

		final Integer invalidPartei = parteienMitId.get(getUniqueParteinameWithQuotation("Ung�ltige"));
		final Integer lastIndex = Integer.valueOf(index);
		kandidatenString.append(",\n('Ung�ltige', 'Ung�ltige', 0, " + invalidPartei + ")");
		for (int i = 1; i <= numberOfWahlkreise; i++) {
			wahlkreisIdAndParteiIdToKandidatId.put(new Pair<Integer, Integer>(Integer.valueOf(i), invalidPartei), lastIndex);
		}

		completeScript.append(parteienString.toString() + ";\n\n");
		completeScript.append(kandidatenString.toString() + ";\n\n");
		completeScript.append(landeslisteString.toString() + ";\n\n");
		completeScript.append(wahlkreislisteString.toString() + ";\n\n");

		parteienWriter.write(parteienString.toString());
		kandidatenWriter.write(kandidatenString.toString());
		landeslisteWriter.write(landeslisteString.toString());
		wahlkreislisteWriter.write(wahlkreislisteString.toString());
	}

	private void parseZweitstimmen(final int year) {
		final String wahlbewerberPath = "data/db_wahl_" + year + "/zweitstimmen.csv";

		final FileReader reader = new FileReader(wahlbewerberPath);
		final List<String> lines = reader.getLines();

		final String zweitstimmenPath = "data/db_wahl_" + year + "/zweitstimmen.sql";

		final FileWriter zweitstimmenWriter = new FileWriter(zweitstimmenPath);

		final StringBuilder zweitstimmenString = new StringBuilder();

		zweitstimmenString.append("insert into Zweitstimmen_aggregiert(wahlkreis_id, partei_id, stimmen) \nvalues \n");

		final Integer yearInt = Integer.valueOf(year);
		int index = 0;
		String parteiName = "";
		for (final String line : lines) {
			final String[] parts = line.replace("\"", "'").split(";", -1);

			final Integer wahlkreis = new Integer(parts[1]);
			parteiName = getUniqueParteinameWithQuotation(parts[2]);
			final Integer partei = parteienMitId.get(parteiName);
			final Integer stimmen = new Integer(parts[3]);
			if (index > 0) {
				zweitstimmenString.append(",\n");
			}
			zweitstimmenString.append("(" + wahlkreisNrAndYearToWahlkreisId.get(new Pair<Integer, Integer>(wahlkreis, yearInt)) + ", " + partei + ", " + stimmen + ")");
			index++;
		}

		completeScript.append(zweitstimmenString.toString() + ";\n\n");

		zweitstimmenWriter.write(zweitstimmenString.toString());
	}

	private void parseErststimmen(final int year) {
		final String erststimmenPath = "data/db_wahl_" + year + "/erststimmen.sql";

		final FileWriter erststimmenWriter = new FileWriter(erststimmenPath);

		final StringBuilder erststimmenString = new StringBuilder();

		erststimmenString.append("insert into Erststimmen_aggregiert(wahlkreis_id, kandidat_id, stimmen) \nvalues \n");
		{
			final String erststimmenInputPath = "data/db_wahl_" + year + "/erststimmen.csv";

			final FileReader reader = new FileReader(erststimmenInputPath);
			final List<String> lines = reader.getLines();

			final Integer yearInt = Integer.valueOf(year);
			int index = 0;
			for (final String line : lines) {
				final String[] parts = line.replace("\"", "'").split(";", -1);

				final Integer wahlkreis = new Integer(parts[1]);
				final Integer parteiId = parteienMitId.get(getUniqueParteinameWithQuotation(parts[2]));
				final Integer stimmen = new Integer(parts[3]);

				if (stimmen.intValue() != 0) {
					final Integer kandidat = wahlkreisIdAndParteiIdToKandidatId.get(new Pair<Integer, Integer>(wahlkreis, parteiId));

					if (index > 0) {
						erststimmenString.append(",\n");
					}
					erststimmenString.append("(" + wahlkreisNrAndYearToWahlkreisId.get(new Pair<Integer, Integer>(wahlkreis, yearInt)) + ", " + kandidat + ", " + stimmen + ")");
					index++;
				}
			}
		}

		{
			final String erststimmenInputPath = "data/db_wahl_" + year + "/erststimmen_parteilos.csv";

			final FileReader reader = new FileReader(erststimmenInputPath);
			final List<String> lines = reader.getLines();

			final Integer yearInt = Integer.valueOf(year);
			for (final String line : lines) {
				final String[] parts = line.replace("\"", "'").split(";", -1);

				final Integer wahlkreis = new Integer(parts[1]);
				final String nachname = parts[2];
				final Integer stimmen = new Integer(parts[3]);

				final Integer kandidat = getKandidatIdFromNachname(nachname, wahlkreis);

				erststimmenString.append(",\n");
				erststimmenString.append("(" + wahlkreisNrAndYearToWahlkreisId.get(new Pair<Integer, Integer>(wahlkreis, yearInt)) + ", " + kandidat + ", " + stimmen + ")");
			}
		}

		completeScript.append(erststimmenString.toString() + ";\n\n");

		erststimmenWriter.write(erststimmenString.toString());
	}

	private String getUniqueParteinameWithQuotation(final String parteiName) {
		if (parteiName == null || parteiName.isEmpty())
			return null;
		String internParteiName = parteiName;
		if (!internParteiName.startsWith("'")) {
			internParteiName = "'" + internParteiName;
		}
		if (!internParteiName.endsWith("'")) {
			internParteiName = internParteiName + "'";
		}
		if (internParteiName.equals("'Volksabst.'")) {
			internParteiName = "'Volksabstimmung'";
		} else if (internParteiName.equals("'�dp'")) {
			internParteiName = "'�DP'";
		} else if (internParteiName.equals("'VIOLETTEN'")) {
			internParteiName = "'DIE VIOLETTEN'";
		} else if (internParteiName.equals("'Tierschutz'") || internParteiName.equals("'Die Tierschutzpartei'")) {
			internParteiName = "'Tierschutzpartei'";
		} else if (internParteiName.equals("'B�ndnis21/RRP'")) {
			internParteiName = "'B�ndnis 21/RRP'";
		}
		return internParteiName;
	}

	private Integer getKandidatIdFromNachname(final String nachname, final Integer wahlkreis) {
		final String nachnameWithQuotation = getStringWithQuotation(nachname);
		if (nachnameWithQuotation.equals("'Schepke F'")) {
			return vornameAndNachnameToKandidatId.get(new Pair<String, String>(getStringWithQuotation("Dr. Frank"), getStringWithQuotation("Schepke")));
		} else if (nachnameWithQuotation.equals("'Schepke M'")) {
			return vornameAndNachnameToKandidatId.get(new Pair<String, String>(getStringWithQuotation("Mark"), getStringWithQuotation("Schepke")));
		} else if (nachnameWithQuotation.equals("'Dieckmann M'")) {
			return vornameAndNachnameToKandidatId.get(new Pair<String, String>(getStringWithQuotation("Mali"), getStringWithQuotation("Dieckmann")));
		} else if (nachnameWithQuotation.equals("'Dieckmann J'")) {
			return vornameAndNachnameToKandidatId.get(new Pair<String, String>(getStringWithQuotation("Jan"), getStringWithQuotation("Dieckmann")));
		}
		return wahlkreisIdAndNachnameToKandidatId.get(new Pair<Integer, String>(wahlkreis, nachnameWithQuotation));
	}

	private String getStringWithQuotation(final String input) {
		if (input == null || input.isEmpty())
			return null;
		String output = input;
		if (!output.startsWith("'")) {
			output = "'" + output;
		}
		if (!output.endsWith("'")) {
			output = output + "'";
		}
		return output;
	}

	private int getBundeslandOffset(final int year) {
		return year == 2009 ? 1 : 17;
	}

	private int getParteiOffset(final int year) {
		return year == 2009 ? 1 : 30;
	}

	private int getKandidatOffset(final int year) {
		return year == 2009 ? 1 : 3558;
	}

	private int getWahlkreisOffset(final int year) {
		return year == 2009 ? 1 : 300;
	}

	// private void convertWahlbewerbert() {
	// String inputPath = "data/db_wahl_2013/wahlbewerber_mit_platz.csv";
	// String outputPath = "data/db_wahl_2013/wahlbewerber.csv";
	//
	// StringBuilder output = new StringBuilder();
	//
	// FileWriter writer = new FileWriter(outputPath);
	// FileReader reader = new FileReader(inputPath);
	// List<String> lines = reader.getLines();
	// for (String line : lines) {
	// String[] parts = line.split(";", -1);
	//
	// String vorname = parts[4];
	// String nachname = parts[3];
	// String jahrgang = parts[5];
	// String partei = parts[6];
	// String wahlkreis = parts[7];
	// String bundesland = parts[8];
	// String listenplatz = parts[9];
	//
	// String titel = parts[2];
	// if (titel != null && !titel.isEmpty()) {
	// vorname = titel + " " + vorname;
	// }
	//
	// output.append(vorname + ";" + nachname + ";" + jahrgang + ";" + partei +
	// ";" + wahlkreis + ";" + bundesland + ";" + listenplatz + "\n");
	// }
	// writer.write(output.toString());
	// }
}
