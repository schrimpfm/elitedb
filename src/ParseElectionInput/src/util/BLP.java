package util;

public class BLP {
	private String bundesland;
	private String partei;

	public BLP(String bundesland, String partei) {
		this.bundesland = bundesland;
		this.partei = partei;

		if (bundesland == null || partei == null) {
			new AssertionError("Bundesland und Partei duerfen nicht leer sein", null);
		}
	}

	public String getBundesland() {
		return bundesland;
	}

	public String getPartei() {
		return partei;
	}
}
