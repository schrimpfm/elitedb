package file;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

public class FileWriter {

	private final String fileName;

	public FileWriter(String fileName) {
		this.fileName = fileName;
	}

	public void write(String text) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new java.io.FileWriter(fileName));
			writer.write(text);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public void writeLines(List<String> lines) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new java.io.FileWriter(fileName));
			for (String line : lines) {
				writer.write(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
