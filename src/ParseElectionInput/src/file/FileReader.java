package file;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReader {
	private final String fileName;

	public FileReader(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getLines() {
		List<String> lines;
		try {
			lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
		} catch (IOException e) {
			return null;
		}
		return lines;
	}
}
