package main;

import parsing.Parser;

public class Main {
	public static void main(String[] args) {
		Parser p = new Parser();
		p.parse();
	}
}
