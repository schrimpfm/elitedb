insert into Bundesland(name, jahr, einwohner) 
values 
('Baden-Württemberg', 2013, 9482902),
('Bayern', 2013, 11353264),
('Berlin', 2013, 3025288),
('Brandenburg', 2013, 2418267),
('Bremen', 2013, 575805),
('Hamburg', 2013, 1559655),
('Hessen', 2013, 5388350),
('Mecklenburg-Vorpommern', 2013, 1585032),
('Niedersachsen', 2013, 7354892),
('Nordrhein-Westfalen', 2013, 15895182),
('Rheinland-Pfalz', 2013, 3672888),
('Saarland', 2013, 919402),
('Sachsen', 2013, 4005278),
('Sachsen-Anhalt', 2013, 2247673),
('Schleswig-Holstein', 2013, 2686085),
('Thüringen', 2013, 2154202)