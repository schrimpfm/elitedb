insert into Bundesland(name, jahr, einwohner) 
values 
('Baden-Württemberg', 2009, 7633818),
('Bayern', 2009, 9382583),
('Berlin', 2009, 2471665),
('Brandenburg', 2009, 2128715),
('Bremen', 2009, 487978),
('Hamburg', 2009, 1256634),
('Hessen', 2009, 4398919),
('Mecklenburg-Vorpommern', 2009, 1400298),
('Niedersachsen', 2009, 6112110),
('Nordrhein-Westfalen', 2009, 13288291),
('Rheinland-Pfalz', 2009, 3103878),
('Saarland', 2009, 808554),
('Sachsen', 2009, 3518195),
('Sachsen-Anhalt', 2009, 2028572),
('Schleswig-Holstein', 2009, 2234720),
('Thüringen', 2009, 1913559)